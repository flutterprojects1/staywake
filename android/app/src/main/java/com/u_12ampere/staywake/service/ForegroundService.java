package com.u_12ampere.staywake.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;

import io.flutter.Log;

import com.u_12ampere.staywake.R;
import com.u_12ampere.staywake.alarm.AlarmCacheFile;
import com.u_12ampere.staywake.receiver.ActionReceiver;


public class ForegroundService extends Service {
    public static final String TAG = ForegroundService.class.getSimpleName();

    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    public static final int NOTIFICATION_ID = 1022;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "ForegroundService onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotificationChannel();

        final PendingIntent actionIntent = PendingIntent.getBroadcast(
                getApplicationContext(),
                1,
                new Intent(getApplicationContext(), ActionReceiver.class),
                0
        );

        StringBuilder builder = new StringBuilder("Du wirst bis ");

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(AlarmCacheFile.getInstance(getApplicationContext()).getLastUntilTimeInMillis());
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                builder.append("Montag");
                break;

            case Calendar.TUESDAY:
                builder.append("Dienstag");
                break;

            case Calendar.WEDNESDAY:
                builder.append("Mittwoch");
                break;

            case Calendar.THURSDAY:
                builder.append("Donnerstag");
                break;

            case Calendar.FRIDAY:
                builder.append("Freitag");
                break;

            case Calendar.SATURDAY:
                builder.append("Samstag");
                break;

            case Calendar.SUNDAY:
                builder.append("Sonntag");
                break;
        }

        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int minute = c.get(Calendar.MINUTE);

        builder.append(' ');

        if (hour < 10) builder.append('0');
        builder.append(hour);

        builder.append(':');

        if (minute < 10) builder.append('0');
        builder.append(minute);

        builder.append(" wachgehalten.");

        startForeground(
                NOTIFICATION_ID,
                getNotification(
                        getString(R.string.label),
                        builder.toString(),
                        null,
                        actionIntent
                )
        );

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "ForegroundService onDestroy");
        super.onDestroy();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    @NonNull
    private Notification getNotification(
            @NonNull String title,
            @Nullable String text,
            @Nullable PendingIntent content,
            @Nullable PendingIntent action
    ) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_notification_human_amber);

        if (null != text)
            builder.setContentText(text);

        if (null != content)
            builder.setContentIntent(content);

        if (null != action) builder.addAction(new NotificationCompat.Action(
                R.drawable.ic_notification_human_amber,
                "Beenden",
                action
        ));

        return builder.build();
    }
}
