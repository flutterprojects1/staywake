package com.u_12ampere.staywake.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import io.flutter.Log;
import io.flutter.plugin.common.MethodCall;

import com.u_12ampere.staywake.Utils;

public class Alarm {

    public static final String TAG = Utils.ROOT_PACKAGE_NAME + Alarm.class.getSimpleName();

    public static final String NATIVE_METHOD_ID = "Alarm:";

    private Context applicationContext;
    private PendingIntent alarmPendingIntent;
    private String alarmId;

    private boolean hasAction;
    private boolean hasStarted;
    private boolean showLog;

    private int type;
    private long triggerTimePoint;

    /**
     * Schedule an alarm to be delivered precisely at the stated time.
     * The alarm will be delivered as nearly as possible to the
     * requested trigger time.
     *
     * <p>
     * <b>Note:</b> only alarms for which there is a strong demand for exact-time
     * delivery (such as an alarm clock ringing at the requested time) should be
     * scheduled as exact.  Applications are strongly discouraged from using exact
     * alarms unnecessarily as they reduce the OS's ability to minimize battery use.
     *
     * @param context The application Context
     */
    public Alarm(Context context, String alarmId) {
        this.applicationContext = context;
        this.alarmId = alarmId;
        hasAction = false;
        hasStarted = false;
        showLog = true;
        type = -1;
        triggerTimePoint = -1;
    }

    public static boolean isAlarmMethod(MethodCall call) {
        final String method = call.method;
        return method.substring(0, Alarm.NATIVE_METHOD_ID.length()).equals(Alarm.NATIVE_METHOD_ID);
    }

    public static String getAlarmMethod(MethodCall call) {
        return call.method.substring(Alarm.NATIVE_METHOD_ID.length());
    }

    public String getAlarmId() {
        return alarmId;
    }

    public String getAlarmIdMessage() {
        return "AlarmId: <" + alarmId + ">";
    }

    private AlarmManager getAlarmManager() {
        return (AlarmManager) applicationContext.getSystemService(Context.ALARM_SERVICE);
    }

    public boolean isHasAction() {
        return hasAction;
    }

    public boolean isHasStarted() {
        return hasStarted;
    }

    public boolean isShowLog() {
        return showLog;
    }

    public Alarm setShowLog(boolean showLog) {
        this.showLog = showLog;
        return this;
    }

    /**
     * @param requestCode int: Private request code for the sender
     * @param flags       int: May be {@link PendingIntent#FLAG_ONE_SHOT},
     *                    {@link PendingIntent#FLAG_NO_CREATE},
     *                    {@link PendingIntent#FLAG_CANCEL_CURRENT},
     *                    {@link PendingIntent#FLAG_UPDATE_CURRENT},
     *                    {@link PendingIntent#FLAG_IMMUTABLE} or any of the
     *                    flags as supported by Intent.fillIn() to control
     *                    which unspecified parts of the intent that can
     *                    be supplied when the actual send happens.
     * @param intent      The receiver, (eg.: new Intent(getApplicationContext, NotificationReceiver.class)).
     * @see PendingIntent
     * @see Intent
     * @see com.u_12ampere.staywake.receiver.notification.NotificationReceiver
     */
    public Alarm setAction(int requestCode, int flags, Intent intent) {
        if (intent == null) {
            Log.e(TAG, getAlarmIdMessage()
                    + "\nAlarm.setAction(" + requestCode
                    + ", " + flags
                    + ", null)\n intent should not be null");
            return null;
        }
        alarmPendingIntent = PendingIntent.getBroadcast(
                applicationContext,
                requestCode,
                intent,
                flags
        );
        hasAction = true;
        hasStarted = false;
        return this;
    }

    /**
     * @param type One of {@link AlarmManager#ELAPSED_REALTIME},
     *             {@link AlarmManager#ELAPSED_REALTIME_WAKEUP},
     *             {@link AlarmManager#RTC}, or {@link AlarmManager#RTC_WAKEUP}.
     */
    public Alarm setType(int type) {
        this.type = type;
        hasStarted = false;
        return this;
    }

    /**
     * The triggerTimePoint is calculated with {@link Utils#getTimeInMillis(int, int, int, boolean)}.
     *
     * @param hour   The hour when you want your Alarm to fire.
     * @param minute The minute on which you want your Alarm to fire.
     * @param second The second on which you want your Alarm to fire.
     * @see Utils#getTimeInMillis(int, int, int, boolean)
     */
    public Alarm pick(int hour, int minute, int second) {
        long _triggerTimePoint = Utils.getTimeInMillis(hour, minute, second, true);
        if (_triggerTimePoint >= 0) triggerTimePoint = _triggerTimePoint;
        hasStarted = false;
        return this;
    }

    /**
     * @param triggerTimePoint The exact point of time in milliseconds, as an offset from
     *                         the <b>Epoch, January 1, 1970 00:00:00.000 GMT (Gregorian)</b>,
     *                         at which you want the Alarm to fire.
     */
    public Alarm pickExact(long triggerTimePoint) {
        if (triggerTimePoint >= 0) this.triggerTimePoint = triggerTimePoint;
        hasStarted = false;
        return this;
    }

    public void start() {
        if (!hasAction) {
            Log.e(TAG, getAlarmIdMessage() + "\nAlarm does not have an action");
            return;
        }

        if (triggerTimePoint < 0) {
            Log.e(TAG, getAlarmIdMessage() + "\nAlarm triggerTimePoint has to be greater, or equal to 0,\n triggerTimePoint: <"
                    + triggerTimePoint + ">\n");
            return;
        }

        if (type < 0) {
            Log.e(TAG, getAlarmIdMessage() + "\nAlarmType has to be within [0;3],\n type: <"
                    + type + ">\n");
            return;
        }

        if (Utils.isMarshmallowOrAbove()) {
            getAlarmManager().setExactAndAllowWhileIdle(
                    type,
                    triggerTimePoint,
                    alarmPendingIntent
            );
        } else {
            getAlarmManager().setExact(
                    type,
                    triggerTimePoint,
                    alarmPendingIntent
            );
        }

        hasStarted = true;
    }

    public void cancel() {
        getAlarmManager().cancel(alarmPendingIntent);
        hasStarted = false;
    }

    public void dispose() {
        if (hasStarted) cancel();
        applicationContext = null;
        alarmPendingIntent = null;
    }
}

class AlarmIdNotFoundException extends Exception {

    Object source;

    /**
     * @param source The source in which the alarmId was searched for
     */
    public AlarmIdNotFoundException(Object source) {
        this.source = source;
    }

    @Override
    public String getMessage() {
        return "AlarmIdNotFoundException: No alarmId was found in source <" + (source != null ? source.toString() : "null") + ">";
    }

}

