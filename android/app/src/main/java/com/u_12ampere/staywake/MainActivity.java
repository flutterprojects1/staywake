package com.u_12ampere.staywake;

import androidx.annotation.NonNull;

import com.u_12ampere.staywake.handler.MethodHandler;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    MethodHandler handler;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        handler = new MethodHandler(flutterEngine, getApplicationContext());
    }
}
