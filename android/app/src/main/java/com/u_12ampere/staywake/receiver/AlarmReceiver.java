package com.u_12ampere.staywake.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import io.flutter.Log;

import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.alarm.Alarm;
import com.u_12ampere.staywake.alarm.AlarmCacheFile;
import com.u_12ampere.staywake.service.ForegroundService;


public class AlarmReceiver extends BroadcastReceiver {
    public static final String LOG_TAG = AlarmReceiver.class.getSimpleName();

    public static final String RECEIVER_KEY = Utils.ROOT_PACKAGE_NAME + ".AlarmReceiverKey";
    public static final String BUNDLE_KEY = Utils.ROOT_PACKAGE_NAME + ".AlarmBundleKey";

    @Override
    public void onReceive(Context context, Intent intent) {

        // Stop notifying the user that this app is running in the background
        if (!setNextAlarm(context)) Utils.stopService(context, ForegroundService.class);


        try {
            ((Receiver) intent
                    .getBundleExtra(BUNDLE_KEY)
                    .getSerializable(RECEIVER_KEY)
            ).onReceive(context, intent.getExtras());
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    /**
     * Returns <b>true</b> if there will be a next alarm, otherwise <b>false</b>.
     */
    private boolean setNextAlarm(Context context) {
        log("setNextAlarm" +
                "\n----------------------------------------------------------------");
        final AlarmCacheFile alarmCacheFile = AlarmCacheFile.getInstance(context);

        log("alarmCacheFile.exists(): " + alarmCacheFile.exists());
        if (!alarmCacheFile.exists()) return false;

        alarmCacheFile.reload();

        log("alarmCacheFile.getJSONReceivers().isEmpty(): " + alarmCacheFile.getJSONReceivers().isEmpty());
        if (alarmCacheFile.getJSONReceivers().isEmpty()) return false;

        final Intent action = Utils.getRandom(
                alarmCacheFile.getJSONReceivers()
        ).getIntent(context);
        log("null == action: " + (null == action));
        if (null == action) return false;

        log("\n\n-----------------------ACTION----------------------------");
        final Bundle extras = action.getExtras();
        for (String s : extras.keySet()) {
            log("KEY: <" + s + ">\tVALUE: <" + extras.get(s) + ">");
        }
        log("\n\n-----------------------ACTION----------------------------");

        // get the alarmId
        log("now getting alarmId...");
        String alarmId = null;
        try {
            alarmId = alarmCacheFile.getAlarmId();
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        if (null == alarmId) return false;
        log("alarmId: " + alarmId);

        Alarm alarm = new Alarm(context, alarmId)
                .setType(alarmCacheFile.getType())
                .setShowLog(alarmCacheFile.getShowLog());
        log("type: " + alarmCacheFile.getType());
        log("showLog: " + alarmCacheFile.getShowLog());

        log("now setting action...");
        // set the action for the alarm
        alarm.setAction(alarmCacheFile.getRequestCode(), alarmCacheFile.getFlag(), action);
        log("requestCode: " + alarmCacheFile.getRequestCode());
        log("flag: " + alarmCacheFile.getFlag());

        log("now picking trigger time");
        // pick trigger time
        final long currentTimeMS = Utils.getCurrentTimeInMillis();
        final long randDelayMS = Utils.randomLong(
                alarmCacheFile.getMinDeltaPeriod(),
                alarmCacheFile.getMaxDeltaPeriod()
        );

        log("currentTimeMS: " + currentTimeMS);
        log("alarmCacheFile.getMinDeltaPeriod(): " + alarmCacheFile.getMinDeltaPeriod());
        log("alarmCacheFile.getMaxDeltaPeriod(): " + alarmCacheFile.getMaxDeltaPeriod());
        log("randDelayMS: " + randDelayMS);
        log("alarmCacheFile.getLastUntilTimeInMillis(): " + alarmCacheFile.getLastUntilTimeInMillis());
        log(" (alarmCacheFile.getLastUntilTimeInMillis() <= currentTimeMS + randDelayMS): " + (alarmCacheFile.getLastUntilTimeInMillis() <= currentTimeMS + randDelayMS));

        // Stop
        if (alarmCacheFile.getLastUntilTimeInMillis() <= currentTimeMS + randDelayMS) return false;

        log("alarm.pickExact(" + (currentTimeMS + randDelayMS) + ").start()");
        alarm.pickExact(currentTimeMS + randDelayMS).start();

        return true;
    }

    private void log(String message) {
        Log.i(LOG_TAG, message + "\n");
    }
}
