package com.u_12ampere.staywake.receiver.flare;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import io.flutter.Log;

import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.receiver.Receiver;

public class FlareReceiver extends Receiver {

    public static final String NATIVE_METHOD_ID = Utils.ROOT_PACKAGE_NAME + "." + FlareReceiver.class.getSimpleName();

    Handler mHandler;
    View filterView;
    int count;
    int maxCount;

    @Override
    public void onReceive(Context context, Bundle bundle) {
        count = 0;
        maxCount = 2000;
        mHandler = new Handler();
        filterView = new LinearLayout(context);
        filterView.setBackgroundColor(Color.argb(100, 255, 180, 255));
        int layoutParameterType;

        if (Utils.isOreoOrAbove()) {
            layoutParameterType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParameterType = WindowManager.LayoutParams.TYPE_PHONE;
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                layoutParameterType,
                280,
                PixelFormat.TRANSLUCENT
        );
        startFilterView(windowManager, layoutParams);
    }

    void startFilterView(WindowManager windowManager, WindowManager.LayoutParams layoutParams) {
        try {
            windowManager.removeView(filterView);
        } catch (Exception e) {
            Log.e(Utils.LOG_TAG_RECEIVER, e.getMessage());
        }
        windowManager.addView(filterView, layoutParams);
        endFilterView(windowManager, layoutParams);
    }

    void endFilterView(WindowManager windowManager, WindowManager.LayoutParams layoutParams) {
        count++;
        mHandler.postDelayed(() -> {
            windowManager.removeView(filterView);
            if (count < maxCount) {
                mHandler.postDelayed(() ->
                                startFilterView(windowManager, layoutParams),
                        50
                );
            }
        }, 150);
    }
}
