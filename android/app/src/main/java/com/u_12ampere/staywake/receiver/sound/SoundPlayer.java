package com.u_12ampere.staywake.receiver.sound;

import android.media.MediaPlayer;

import io.flutter.Log;

public class SoundPlayer {
    public static final String LOG_TAG = SoundPlayer.class.getSimpleName();

    private MediaPlayer player;
    private boolean hasData;

    public SoundPlayer() {
        player = new MediaPlayer();
        hasData = false;
    }

    public SoundPlayer setData(String path) {
        try {
            player.setDataSource(path);
            player.prepare();
            hasData = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error on 'setData(" + path + ")'\nError message: " + e.getMessage());
        }
        return this;
    }

    public MediaPlayer getPlayer() {
        return player;
    }

    public int getDurationInMillis() {
        return player.getDuration();
    }

    public void play() {
        if (hasData) player.start();
    }

    public void pause() {
        if (hasData) player.pause();
    }

    public void stop() {
        if (hasData) player.stop();
    }

    public void dispose() {
        if (player != null) {
            if (player.isPlaying()) player.stop();
            player.release();
            player = null;
        }
    }

}