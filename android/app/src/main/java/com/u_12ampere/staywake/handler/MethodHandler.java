package com.u_12ampere.staywake.handler;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Objects;

import io.flutter.Log;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.alarm.Alarm;
import com.u_12ampere.staywake.alarm.AlarmCacheFile;
import com.u_12ampere.staywake.alarm.AlarmIntent;
import com.u_12ampere.staywake.receiver.Receiver;
import com.u_12ampere.staywake.receiver.flare.FlareReceiver;
import com.u_12ampere.staywake.receiver.notification.NotificationReceiver;
import com.u_12ampere.staywake.receiver.sound.SoundReceiver;
import com.u_12ampere.staywake.receiver.video.VideoReceiver;
import com.u_12ampere.staywake.service.ForegroundService;

public final class MethodHandler implements MethodChannel.MethodCallHandler {

    public static final String TAG = MethodHandler.class.getSimpleName();
    public static final String CHANNEL_ID = Utils.ROOT_PACKAGE_NAME + "/staywake";

    private final Context applicationContext;
    private final MethodChannel channel;
    private Intent action;
    private int logLevel;

    private HashMap<String, Alarm> alarms;

    public MethodHandler(FlutterEngine flutterEngine, Context applicationContext) {
        this.applicationContext = applicationContext;

        channel = new MethodChannel(
                flutterEngine.getDartExecutor().getBinaryMessenger(),
                CHANNEL_ID
        );

        alarms = new HashMap<>();

        channel.setMethodCallHandler(this);

        logLevel = 0;

        Log.setLogLevel(logLevel);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        Log.i(TAG, "onMethodCall"
                + "\nmethod: " + call.method
                + "\nargs: " + call.arguments);

        boolean isCallHandled = false;

        isCallHandled = switchAlarmCall(call, result);
        if (!isCallHandled) isCallHandled = switchReceiverCall(call, result);
        if (!isCallHandled) isCallHandled = switchDebugMethod(call, result);

        if (!isCallHandled) result.notImplemented();

    }

    private boolean switchDebugMethod(MethodCall call, MethodChannel.Result result) {
        switch (call.method) {
            case "print_cache":
                AlarmCacheFile.getInstance(applicationContext).logData();
                break;

            case "reload_cache":
                AlarmCacheFile.getInstance(applicationContext).reload();
                break;

            case "print_CacheDir":
                Log.i(TAG, "---------------------------------------\n");
                for (String s : Objects.requireNonNull(applicationContext.getCacheDir().list()))
                    Log.i(TAG, "getCacheDir: " + s);
                Log.i(TAG, "---------------------------------------");
                break;

            case "MethodHandler_setGlobalShowLog":
                if (call.argument("gloablShowLog")) {
                    logLevel = 0;
                } else {
                    logLevel = 10;
                }
                break;

            case "MethodHandler_getGlobalShowLog":
                result.success(0 == logLevel);
                break;

            default:
                return false;
        }

        return true;
    }

    private boolean switchAlarmCall(MethodCall call, MethodChannel.Result result) {
        if (Alarm.isAlarmMethod(call)) {

            final String alarmMethod = Alarm.getAlarmMethod(call);
            final String alarmId = call.argument("alarmId");

            Log.i(TAG, "\nalarmId: <" + alarmId
                    + ">\nAlarm Method: <" + alarmMethod + ">");

            switch (alarmMethod) {
                case "constructor":
                    final int initType = call.argument("type");
                    final boolean initShowLog = call.argument("showLog");

                    if (!alarms.containsKey(alarmId)) {
                        Alarm initAlarm = new Alarm(applicationContext, alarmId);
                        initAlarm.setShowLog(initShowLog);
                        initAlarm.setType(initType);
                        alarms.put(alarmId, initAlarm);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There already exists an Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There already exists an Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "isHasAction":
                    if (alarms.containsKey(alarmId)) {
                        result.success(alarms.get(alarmId).isHasAction());
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "isHasStarted":
                    if (alarms.containsKey(alarmId)) {
                        result.success(alarms.get(alarmId).isHasStarted());
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "setShowLog":
                    final boolean showLog = call.argument("showLog");

                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).setShowLog(showLog);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "setAction":
                    final int requestCode = call.argument("requestCode");
                    final int flags = call.argument("flags");

                    Log.i(TAG, "\nrequestCode: <" + requestCode
                            + ">\nflags: <" + flags
                            + ">\n");

                    Log.i(TAG, "Action Extras: " + action.getExtras().toString());

                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).setAction(requestCode, flags, action);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "setType":
                    final int type = call.argument("type");

                    Log.i(TAG, "\nsetting type: <" + type + ">\n");

                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).setType(type);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "pick":
                    final int hours = call.argument("hours");
                    final int minutes = call.argument("minutes");

                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).pick(hours, minutes, 0);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "pickExact":
                    final long timePoint = call.argument("timePoint");

                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).pickExact(timePoint);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "start":
                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).start();
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                // This case is nearly identical to "start"
                // with the only difference being that
                // we start a foreground service here.
                case "startChainReaction":
                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).start();
                        Utils.startService(applicationContext, ForegroundService.class);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;


                case "cancel":
                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).cancel();
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;

                case "dispose":
                    if (alarms.containsKey(alarmId)) {
                        alarms.get(alarmId).dispose();
                        alarms.remove(alarmId);
                        result.success(null);
                    } else {
                        Log.e(TAG, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n");

                        result.error(call.method, "MethodCall: <" + call.method + ">\n"
                                + "There is no Alarm with the alarmId: <"
                                + alarmId + ">\n", null);
                    }
                    break;
            }

            return true;
        }

        return false;
    }

    private boolean switchReceiverCall(MethodCall call, MethodChannel.Result result) {

        if (call.method.equals(Receiver.getNativeMethodId(NotificationReceiver.class))) {
            // NotificationReceiver

            final String title = call.argument(NotificationReceiver.TITLE_KEY);
            final String message = call.argument(NotificationReceiver.MESSAGE_KEY);
            final String channelId = call.argument(NotificationReceiver.CHANNEL_ID_KEY);
            final int notificationId = call.argument(NotificationReceiver.NOTIFICATION_ID_KEY);

            Log.i(TAG, "\ntitle: <" + title
                    + ">\nmessage: <" + message
                    + ">\nchannelId: <" + channelId
                    + ">\nnotificationId: <" + notificationId
                    + ">");

            action = new AlarmIntent(applicationContext)
                    .putReceiver(new NotificationReceiver())
                    .putExtra(NotificationReceiver.TITLE_KEY, title)
                    .putExtra(NotificationReceiver.MESSAGE_KEY, message)
                    .putExtra(NotificationReceiver.CHANNEL_ID_KEY, channelId)
                    .putExtra(NotificationReceiver.NOTIFICATION_ID_KEY, notificationId)
                    .getIntent();
//******************************************************************************
        } else if (call.method.equals(Receiver.getNativeMethodId(SoundReceiver.class))) {
            // SoundReceiver

            final String soundPath = call.argument(SoundReceiver.SOUND_KEY_PATH);
            final double soundDurationFactor = call.argument(SoundReceiver.SOUND_KEY_DURATION_FACTOR);
            final boolean soundCloseWhenCompleted = call.argument(SoundReceiver.SOUND_KEY_CLOSE_WHEN_COMPLETED);

            Log.i(TAG, "\nsoundpath: <" + soundPath
                    + ">\nsoundDurationFactor: <" + soundDurationFactor
                    + ">\nsoundCloseWhenCompleted: <" + soundCloseWhenCompleted
                    + ">");

            action = new AlarmIntent(applicationContext)
                    .putReceiver(new SoundReceiver())
                    .putExtra(SoundReceiver.SOUND_KEY_PATH, soundPath)
                    .putExtra(SoundReceiver.SOUND_KEY_DURATION_FACTOR, soundDurationFactor)
                    .putExtra(SoundReceiver.SOUND_KEY_CLOSE_WHEN_COMPLETED, soundCloseWhenCompleted)
                    .getIntent();

//******************************************************************************
        } else if (call.method.equals(Receiver.getNativeMethodId(VideoReceiver.class))) {
            // VideoReceiver

            final String videoPath = call.argument(VideoReceiver.VIDEO_KEY_PATH);
            final double videoDurationFactor = call.argument(VideoReceiver.VIDEO_KEY_DURATION);
            final boolean videoCloseWhenCompleted = call.argument(VideoReceiver.VIDEO_KEY_CLOSE_WHEN_COMPLETED);

            Log.i(TAG, "\nvideoPath: <" + videoPath
                    + ">\nvideoDurationFactor: <" + videoDurationFactor + ">");

            action = new AlarmIntent(applicationContext)
                    .putReceiver(new VideoReceiver())
                    .putExtra(VideoReceiver.VIDEO_KEY_PATH, videoPath)
                    .putExtra(VideoReceiver.VIDEO_KEY_DURATION, videoDurationFactor)
                    .putExtra(VideoReceiver.VIDEO_KEY_CLOSE_WHEN_COMPLETED, videoCloseWhenCompleted)
                    .getIntent();

//******************************************************************************
        } else if (call.method.equals(Receiver.getNativeMethodId(FlareReceiver.class))) {
            // FlareReceiver
            // TODO: Implement this
            action = new AlarmIntent(applicationContext)
                    .putReceiver(new FlareReceiver())
                    .getIntent();

//******************************************************************************
        } else {
            return false;
        }

        result.success(null);

        return true;
    }

}
