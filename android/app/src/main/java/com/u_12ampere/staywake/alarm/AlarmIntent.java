package com.u_12ampere.staywake.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.u_12ampere.staywake.receiver.AlarmReceiver;
import com.u_12ampere.staywake.receiver.Receiver;

public class AlarmIntent {
    private final Intent intent;
    private Bundle serializableBundle;

    public AlarmIntent(Context context) {
        intent = new Intent(context, AlarmReceiver.class);
    }

    private Bundle getSerializableBundle() {
        if (serializableBundle == null) serializableBundle = new Bundle();
        return serializableBundle;
    }

    public Intent getIntent() {
        if (serializableBundle != null)
            intent.putExtra(AlarmReceiver.BUNDLE_KEY, serializableBundle);
        return intent;
    }

    public AlarmIntent putReceiver(Receiver receiver) {
        getSerializableBundle().putSerializable(AlarmReceiver.RECEIVER_KEY, receiver);
        return this;
    }

    public AlarmIntent setClass(Context context, Class<?> cls) {
        intent.setClass(context, cls);
        return this;
    }

    public AlarmIntent putExtra(String name, int value) {
        intent.putExtra(name, value);
        return this;
    }

    public AlarmIntent putExtra(String name, long value) {
        intent.putExtra(name, value);
        return this;
    }

    public AlarmIntent putExtra(String name, double value) {
        intent.putExtra(name, value);
        return this;
    }

    public AlarmIntent putExtra(String name, boolean value) {
        intent.putExtra(name, value);
        return this;
    }

    public AlarmIntent putExtra(String name, String value) {
        intent.putExtra(name, value);
        return this;
    }
}
