package com.u_12ampere.staywake.receiver;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import java.io.Serializable;

import com.u_12ampere.staywake.Utils;

public abstract class Receiver implements Serializable {
    public abstract void onReceive(Context context, Bundle bundle);

    @NonNull
    public static <T extends Receiver> String getNativeMethodId(@NonNull Class<T> receiver) {
        return Utils.ROOT_PACKAGE_NAME + "." + receiver.getSimpleName();
    }

    @NonNull
    protected static <T extends Receiver> String getKey(@NonNull Class<T> receiver, @NonNull String subfix) {
        return getNativeMethodId(receiver) + subfix;
    }
}
