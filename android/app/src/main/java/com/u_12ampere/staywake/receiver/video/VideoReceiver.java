package com.u_12ampere.staywake.receiver.video;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;

import static android.view.WindowManager.LayoutParams;

import io.flutter.Log;

import com.u_12ampere.staywake.R;
import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.receiver.Receiver;


public class VideoReceiver extends Receiver {

    public static final String VIDEO_KEY_PATH = getKey(VideoReceiver.class, "path");
    public static final String VIDEO_KEY_DURATION = getKey(VideoReceiver.class, "durationFactor");
    public static final String VIDEO_KEY_CLOSE_WHEN_COMPLETED = getKey(VideoReceiver.class, "closeWhenCompleted");

    @Override
    public void onReceive(Context context, Bundle bundle) {
        final String path = bundle.getString(VIDEO_KEY_PATH);
        final double durationFactor = bundle.getDouble(VIDEO_KEY_DURATION, 1);
        final boolean closeWhenCompleted = bundle.getBoolean(VIDEO_KEY_CLOSE_WHEN_COMPLETED, true);

        final Handler handler = new Handler();

        Log.i(Utils.LOG_TAG_RECEIVER, getNativeMethodId(VideoReceiver.class)
                + "\npath: <" + path
                + ">\ndurationFactor: <" + durationFactor
                + ">\ncloseWhenCompleted: <" + closeWhenCompleted
                + ">");

        // Configure layoutParameterType
        int layoutParameterType;
        if (Utils.isOreoOrAbove()) {
            layoutParameterType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParameterType = WindowManager.LayoutParams.TYPE_PHONE;
        }

        // Get WindowManager
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        LayoutParams windowManagerLayoutParameters = new WindowManager.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT,
                layoutParameterType,
                280,
                PixelFormat.TRANSLUCENT
        );

        // Get layout from context
        View parentView = View.inflate(context, R.layout.video_receiver, null);
        FastVideoView videoView = (FastVideoView) parentView.findViewById(R.id.video_view);

        videoView.setDataSource(path);
        videoView.prepare(false);
        videoView.setFastSurfaceTextureListener(new FastSurfaceTextureListener() {
            @Override
            public void onAvailable(SurfaceTexture surfaceTexture) {
                videoView.start();
            }

            @Override
            public void onSizeChanged(SurfaceTexture surfaceTexture) {

            }

            @Override
            public void onDestroyed(SurfaceTexture surfaceTexture) {

            }

            @Override
            public void onUpdated(SurfaceTexture surfaceTexture) {

            }
        });

        // Handle the removal of the videoView
        final Runnable removeVideoView = () -> windowManager.removeView(parentView);
        if (closeWhenCompleted || 1 <= durationFactor) {
            videoView.setOnCompletionListener(mp -> {
                Log.i(Utils.LOG_TAG_RECEIVER, "onCompletion, duration: <" + mp.getDuration() + ">");
                mp.stop();
                handler.post(removeVideoView);
            });
        } else {
            final int duration = (int) (videoView.getPlayer().getDuration() * durationFactor);
            // Close video manually
            videoView.getPlayer().stop();
            handler.postDelayed(removeVideoView, duration);
        }

        // Add videoView to window
        handler.post(() -> windowManager.addView(parentView, windowManagerLayoutParameters));
    }
}

