package com.u_12ampere.staywake.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.flutter.Log;

import com.u_12ampere.staywake.alarm.AlarmCacheFile;
import com.u_12ampere.staywake.service.ForegroundService;

public class ActionReceiver extends BroadcastReceiver {

    public static final String TAG = ActionReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "In ActionReceiver.onReceive");

        // Stop the notification
        context.stopService(new Intent(context, ForegroundService.class));

        // Cancel the next alarm, if there is any
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final AlarmCacheFile alarmCacheFile = AlarmCacheFile.getInstance(context);
        final PendingIntent nextAlarmAction = PendingIntent.getBroadcast(
                context,
                alarmCacheFile.getRequestCode(),
                new Intent(context, AlarmReceiver.class),
                alarmCacheFile.getFlag()
        );

        // This should work, because no Extra Data
        // is compared when comparing intents.
        alarmManager.cancel(nextAlarmAction);

        Log.i(TAG, "Stopped ForegroundService!");
    }
}
