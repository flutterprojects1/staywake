package com.u_12ampere.staywake.receiver;

import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

import io.flutter.Log;

import com.u_12ampere.staywake.alarm.AlarmCacheFile;
import com.u_12ampere.staywake.alarm.AlarmIntent;
import com.u_12ampere.staywake.receiver.notification.NotificationReceiver;
import com.u_12ampere.staywake.receiver.sound.SoundReceiver;
import com.u_12ampere.staywake.receiver.video.VideoReceiver;


public class JSONReceiver {
    public static final String LOG_TAG = JSONReceiver.class.getSimpleName();

    public static final String NOTIFICATION = "NotificationReceiver";
    public static final String SOUND = "SoundReceiver";
    public static final String VIDEO = "VideoReceiver";

    private JSONObject data;

    public JSONReceiver(JSONObject data) {
        this.data = data;
        Log.i(LOG_TAG, "\n\n----------------------------------------\n\n" + data.toString() + "\n----------------------------------------\n");
    }

    public String getReceiverId() {
        try {
            if (data.has(AlarmCacheFile.RECEIVER_ID))
                return data.getString(AlarmCacheFile.RECEIVER_ID);
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }

        return null;
    }

    public Intent getIntent(Context context) {
        try {
            final AlarmIntent intent = new AlarmIntent(context);
            Log.i(LOG_TAG, "id: " + getReceiverId());

            switch (getReceiverId()) {
                case NOTIFICATION:
                    Log.i(LOG_TAG, "ReceiverId is: <" + NOTIFICATION + ">\n");
                    intent
                            .putReceiver(new NotificationReceiver())
                            .putExtra(NotificationReceiver.TITLE_KEY, data.getString(NotificationReceiver.TITLE_KEY))
                            .putExtra(NotificationReceiver.MESSAGE_KEY, data.getString(NotificationReceiver.MESSAGE_KEY))
                            .putExtra(NotificationReceiver.CHANNEL_ID_KEY, data.getString(NotificationReceiver.CHANNEL_ID_KEY))
                            .putExtra(NotificationReceiver.NOTIFICATION_ID_KEY, data.getInt(NotificationReceiver.NOTIFICATION_ID_KEY));
                    break;

                case SOUND:
                    Log.i(LOG_TAG, "ReceiverId is: <" + SOUND + ">\n");
                    intent
                            .putReceiver(new SoundReceiver())
                            .putExtra(SoundReceiver.SOUND_KEY_PATH, data.getString(SoundReceiver.SOUND_KEY_PATH))
                            .putExtra(SoundReceiver.SOUND_KEY_DURATION_FACTOR, data.getDouble(SoundReceiver.SOUND_KEY_DURATION_FACTOR))
                            .putExtra(SoundReceiver.SOUND_KEY_CLOSE_WHEN_COMPLETED, data.getBoolean(SoundReceiver.SOUND_KEY_CLOSE_WHEN_COMPLETED));
                    break;

                case VIDEO:
                    Log.i(LOG_TAG, "ReceiverId is: <" + VIDEO + ">\n");
                    intent
                            .putReceiver(new VideoReceiver())
                            .putExtra(VideoReceiver.VIDEO_KEY_PATH, data.getString(VideoReceiver.VIDEO_KEY_PATH))
                            .putExtra(VideoReceiver.VIDEO_KEY_DURATION, data.getDouble(VideoReceiver.VIDEO_KEY_DURATION))
                            .putExtra(VideoReceiver.VIDEO_KEY_CLOSE_WHEN_COMPLETED, data.getBoolean(VideoReceiver.VIDEO_KEY_CLOSE_WHEN_COMPLETED));
                    break;

                default:
                    return null;
            }

            return intent.getIntent();
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        return null;
    }

    @Override
    public String toString() {
        return "JSONReceiver: {"
                + AlarmCacheFile.RECEIVER_ID + ": " + getReceiverId() + ", "
                + "data: " + data.toString()
                + "}";
    }
}