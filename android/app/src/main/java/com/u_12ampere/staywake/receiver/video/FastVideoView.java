package com.u_12ampere.staywake.receiver.video;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;

import java.io.IOException;

import io.flutter.Log;

public class FastVideoView extends TextureView implements TextureView.SurfaceTextureListener {

    public static final String LOG_TAG = FastVideoView.class.getSimpleName();

    private Uri mDataSource;
    private MediaPlayer mMediaPlayer;
    private boolean _hasSurfaceMediaPlayer;

    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener;
    private MediaPlayer.OnPreparedListener mPreparedListener;
    private MediaPlayer.OnErrorListener mErrorListener;
    private MediaPlayer.OnInfoListener mInfoListener;
    private MediaPlayer.OnCompletionListener mCompletionListener;

    private FastSurfaceTextureListener fastSurfaceTextureListener;

    public FastVideoView(Context context) {
        this(context, null, 0);
    }

    public FastVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FastVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setSurfaceTextureListener(this);
        mMediaPlayer = new MediaPlayer();
        _hasSurfaceMediaPlayer = false;
    }

    public void setDataSource(String source) {
        setDataSource(Uri.parse(source));
    }

    public void setDataSource(Uri source) {
        mDataSource = source;
        try {
            mMediaPlayer.setDataSource(getContext(), mDataSource);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    public void setOnBufferingUpdateListener(MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener) {
        this.mBufferingUpdateListener = mBufferingUpdateListener;
        mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener mPreparedListener) {
        this.mPreparedListener = mPreparedListener;
        mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener mErrorListener) {
        this.mErrorListener = mErrorListener;
        mMediaPlayer.setOnErrorListener(this.mErrorListener);
    }

    public void setOnInfoListener(MediaPlayer.OnInfoListener mInfoListener) {
        this.mInfoListener = mInfoListener;
        mMediaPlayer.setOnInfoListener(this.mInfoListener);
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener mCompletionListener) {
        this.mCompletionListener = mCompletionListener;
        mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
    }

    public void setFastSurfaceTextureListener(FastSurfaceTextureListener listener) {
        this.fastSurfaceTextureListener = listener;
    }

    public MediaPlayer getPlayer() {
        return mMediaPlayer;
    }

    public boolean hasSurfaceMediaPlayer() {
        return _hasSurfaceMediaPlayer;
    }

    public void start() {
        mMediaPlayer.start();
    }

    public void stop() {
        mMediaPlayer.stop();
    }

    public void prepare(boolean async) {
        if (async) mMediaPlayer.prepareAsync();
        else {
            try {
                mMediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        // release resources on detach
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        super.onDetachedFromWindow();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        fastSurfaceTextureListener.onAvailable(surfaceTexture);
        Surface surface = new Surface(surfaceTexture);
        try {
            mMediaPlayer.setSurface(surface);
            _hasSurfaceMediaPlayer = true;
        } catch (IllegalArgumentException | SecurityException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, e.getMessage());
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, e.getMessage());
            mMediaPlayer.reset();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        fastSurfaceTextureListener.onSizeChanged(surface);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        surface.release();
        fastSurfaceTextureListener.onDestroyed(surface);
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        fastSurfaceTextureListener.onUpdated(surface);
    }
}

interface FastSurfaceTextureListener {
    public void onAvailable(SurfaceTexture surfaceTexture);

    public void onSizeChanged(SurfaceTexture surfaceTexture);

    public void onDestroyed(SurfaceTexture surfaceTexture);

    public void onUpdated(SurfaceTexture surfaceTexture);
}
