package com.u_12ampere.staywake.receiver.sound;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import io.flutter.Log;

import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.receiver.Receiver;

public class SoundReceiver extends Receiver {

    public static final String SOUND_KEY_PATH = getKey(SoundReceiver.class, "path");
    public static final String SOUND_KEY_DURATION_FACTOR = getKey(SoundReceiver.class, "durationFactor");
    public static final String SOUND_KEY_CLOSE_WHEN_COMPLETED = getKey(SoundReceiver.class, "closeWhenCompleted");

    @Override
    public void onReceive(Context context, Bundle bundle) {
        final String path = bundle.getString(SOUND_KEY_PATH);
        final double durationFactor = bundle.getDouble(SOUND_KEY_DURATION_FACTOR, 1);
        final boolean closeWhenCompleted = bundle.getBoolean(SOUND_KEY_CLOSE_WHEN_COMPLETED, true);

        final SoundPlayer player = new SoundPlayer().setData(path);

        final int duration = (int) (player.getDurationInMillis() * durationFactor);

        Log.i(Utils.LOG_TAG_RECEIVER, getNativeMethodId(SoundReceiver.class)
                + "\npath: <" + path
                + ">\ndurationFactor: <" + durationFactor
                + ">\ncloseWhenCompleted: <" + closeWhenCompleted
                + ">\nreal durationInMillis: <" + player.getDurationInMillis()
                + ">\ndurationInMillis: <" + duration
                + ">");

        // Handle the removal of the sound
        if (closeWhenCompleted || 1 <= durationFactor) {
            player.getPlayer().setOnCompletionListener(mp -> player.dispose());
        } else {
            Handler h = new Handler();
            h.postDelayed(() -> {
                player.stop();
                player.dispose();
            }, duration);
        }

        player.play();
    }
}
