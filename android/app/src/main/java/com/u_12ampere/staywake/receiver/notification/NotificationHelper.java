package com.u_12ampere.staywake.receiver.notification;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;

import io.flutter.Log;

import com.u_12ampere.staywake.R;
import com.u_12ampere.staywake.Utils;

public final class NotificationHelper {

    @TargetApi(Build.VERSION_CODES.O)
    public static final int default_Channel_Importance = NotificationManager.IMPORTANCE_DEFAULT;
    public static final String default_Channel_Id = "test_alarm_manager";
    public static final String default_Channel_Name = "test_alarm_manager channel";

    private static ArrayList<NotificationChannel> channels = new ArrayList<>();

    private static boolean isChannelsCreated = false;
    private static NotificationManager manager;

    public static void initialize(Context context) {
        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Utils.isOreoOrAbove()) {
            addChannel(
                    new NotificationChannel(
                            default_Channel_Id,
                            default_Channel_Name,
                            default_Channel_Importance
                    )
            );
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void addChannel(NotificationChannel channel) {
        channels.add(channel);
        isChannelsCreated = false;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void removeChannel(NotificationChannel channel) {
        channels.remove(channel);
        isChannelsCreated = false;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void removeChannel(int index) {
        channels.remove(index);
        isChannelsCreated = false;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static ArrayList<NotificationChannel> getChannels() {
        return channels;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static String getChannelId(int index) {
        return channels.get(index).getId();
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static NotificationChannel findChannelById(String id) {
        for (NotificationChannel channel : channels) {
            if (channel.getId().equals(id)) return channel;
        }
        return null;
    }

    public static void createChannels() {
        if (Utils.isOreoOrAbove()) {
            for (int i = 0; i < channels.size(); i++) {
                manager.createNotificationChannel(channels.get(i));
            }
        }
        isChannelsCreated = true;
    }

    public static NotificationManager getManager() {
        return manager;
    }

    /**
     * @param channelId The {@link NotificationChannel} ID, will be ignored below Android Oreo
     *                  ({@link Build.VERSION_CODES#O}, SDK level 26)
     */
    public static NotificationCompat.Builder getNotification(
            Context applicationContext,
            String channelId,
            String title,
            String msg
    ) {
        if (!isChannelsCreated) createChannels();
        Log.i(NotificationReceiver.class.getSimpleName(), "title: " + title + "\nid: " + channelId + "\nmsg: " + msg);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(applicationContext, channelId);

        builder.setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_android);

        return builder;
    }
}
