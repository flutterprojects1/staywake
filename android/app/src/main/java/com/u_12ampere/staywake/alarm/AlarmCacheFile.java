package com.u_12ampere.staywake.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;

import androidx.annotation.StringDef;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.flutter.Log;

import com.u_12ampere.staywake.receiver.JSONReceiver;

public class AlarmCacheFile {
    public static final String TAG = AlarmCacheFile.class.getSimpleName();
    public static final String FILE_NAME = "AlarmDataCache.json";

    private static AlarmCacheFile instance = null;

    /**
     * This method is threadsafe.
     */
    public static AlarmCacheFile getInstance(Context context) {
        synchronized (AlarmCacheFile.class) {
            if (null == instance)
                instance = new AlarmCacheFile(context);
        }

        return instance;
    }

    private JSONObject jsonAlarmData;
    private final File jsonFile;

    private AlarmCacheFile(Context context) {
        String filePath = context.getCacheDir().getPath() + File.separator + FILE_NAME;
        Log.i(TAG, "filePath: <" + filePath + ">");
        jsonFile = new File(filePath);
        if (jsonFile.exists()) fillJsonObjects(jsonFile);
    }

    private String readFile(File file) {
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                fileData.append(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return fileData.toString();
    }

    private void fillJsonObjects(File src) {
        try {
            jsonAlarmData = new JSONObject(readFile(src));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void logNotExists(String property) {
        Log.e(TAG, "Can not get the property: <" + property + ">, because the " + FILE_NAME + ", does not exist.");
    }

    /**
     * Reload the contents of the AlarmDataCache.json file from the storage, if
     * it exists.
     */
    public void reload() {
        if (exists()) fillJsonObjects(jsonFile);
    }

    /**
     * Whether the AlarmCacheFile.json exists on the Users local storage
     * or not.
     */
    public boolean exists() {
        return jsonFile.exists();
    }

    public void logData() {
        Log.i(TAG, "****************** AlarmCacheFile.logData() ******************\n");
        Log.i(TAG, "EXISTS: " + exists());
        Log.i(TAG, "jsonAlarmData is null: " + (null == jsonAlarmData));

        if (null != jsonAlarmData) {
            Log.i(TAG, "Hours: " + getHours());
            Log.i(TAG, "Minutes: " + getMinutes());
            Log.i(TAG, "Type: " + getType());
            Log.i(TAG, "ShowLog: " + getShowLog());
            Log.i(TAG, "RequestCode: " + getRequestCode());
            Log.i(TAG, "Flag: " + getFlag());
            Log.i(TAG, "MaxDeltaPeriod: " + getMaxDeltaPeriod());
            Log.i(TAG, "MinDeltaPeriod: " + getMinDeltaPeriod());
            Log.i(TAG, "LastUntilTimeInMillis: " + getLastUntilTimeInMillis());

            try {
                Log.i(TAG, "AlarmId: " + getAlarmId());
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

            Log.i(TAG, "\n\n\n");
            for (JSONReceiver jr : getJSONReceivers())
                Log.i(TAG, jr.toString() + "\n\n\n");
        }

        Log.i(TAG, "**************************************************************");
    }

    /**
     * Returns the hour until which the user wants to stay awake.
     * <br></br><br></br>
     * <b>Returns -1 if not found.</b>
     */
    public int getHours() {
        if (!exists()) {
            logNotExists(HOURS);
            return -1;
        }
        int hours = -1;
        try {
            if (jsonAlarmData.has(HOURS)) hours = jsonAlarmData.getInt(HOURS);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return hours;
    }

    /**
     * Returns the minutes until which the user wants to stay awake.
     * <br></br><br></br>
     * <b>Returns -1 if not found.</b>
     */
    public int getMinutes() {
        if (!exists()) {
            logNotExists(MINUTES);
            return -1;
        }
        int minutes = -1;
        try {
            if (jsonAlarmData.has(MINUTES)) minutes = jsonAlarmData.getInt(MINUTES);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return minutes;
    }

    /**
     * Returns type of alarm, one of the following
     * <br></br>
     * {@link AlarmManager#ELAPSED_REALTIME},
     * {@link AlarmManager#ELAPSED_REALTIME_WAKEUP},
     * {@link AlarmManager#RTC}, or {@link AlarmManager#RTC_WAKEUP}.
     * <br></br><br></br>
     * <b>Returns {@link AlarmManager#RTC_WAKEUP} if not found.</b>
     *
     * @see AlarmManager
     */
    public int getType() {
        if (!exists()) {
            logNotExists(TYPE);
            return AlarmManager.RTC_WAKEUP;
        }
        int type = AlarmManager.RTC_WAKEUP;
        try {
            if (jsonAlarmData.has(TYPE)) type = jsonAlarmData.getInt(TYPE);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return type;
    }

    /**
     * Returns if log for the alarm should be shown.
     * <br></br><br></br>
     * <b>Returns false if not found.</b>
     */
    public boolean getShowLog() {
        if (!exists()) {
            logNotExists(SHOW_LOG);
            return false;
        }
        boolean showLog = false;
        try {
            if (jsonAlarmData.has(SHOW_LOG)) showLog = jsonAlarmData.getBoolean(SHOW_LOG);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return showLog;
    }

    /**
     * Returns the alarm id.
     */
    public String getAlarmId() throws AlarmIdNotFoundException {
        if (!exists()) {
            logNotExists(AlARM_ID);
            throw new AlarmIdNotFoundException(jsonFile);
        }
        String alarmId = null;
        try {
            if (jsonAlarmData.has(AlARM_ID)) {
                alarmId = jsonAlarmData.getString(AlARM_ID);
            } else {
                throw new AlarmIdNotFoundException(jsonAlarmData);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return alarmId;
    }

    /**
     * Returns a List of all possible {@link JSONReceiver} for the {@link AlarmReceiver}
     * <br></br><br></br>
     *
     * @see JSONReceiver
     * @see AlarmReceiver
     */
    public ArrayList<JSONReceiver> getJSONReceivers() {
        ArrayList<JSONReceiver> jsonReceivers = new ArrayList<>();

        if (!exists()) {
            logNotExists(RECEIVERS);
            return jsonReceivers;
        }

        try {
            JSONArray receiversData = jsonAlarmData.getJSONArray(RECEIVERS);

            for (int i = 0; i < receiversData.length(); i++)
                jsonReceivers.add(new JSONReceiver(receiversData.getJSONObject(i)));

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return jsonReceivers;
    }

    /**
     * Returns the requestCode for the {@link PendingIntent#getBroadcast}.
     * <br></br><br></br>
     * <b>Returns 0 if not found.</b>
     *
     * @see PendingIntent
     */
    public int getRequestCode() {
        if (!exists()) {
            logNotExists(REQUESTCODE);
            return 0;
        }
        int requestCode = 0;
        try {
            if (jsonAlarmData.has(REQUESTCODE)) requestCode = jsonAlarmData.getInt(REQUESTCODE);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return requestCode;
    }

    /**
     * Returns the flag for the PendingIntent, one of the following
     * <br></br>
     * {@link PendingIntent#FLAG_CANCEL_CURRENT},
     * {@link PendingIntent#FLAG_IMMUTABLE},
     * {@link PendingIntent#FLAG_NO_CREATE},
     * {@link PendingIntent#FLAG_ONE_SHOT}, or {@link PendingIntent#FLAG_UPDATE_CURRENT}.
     * <br></br><br></br>
     * <b>Returns 0, which stands for no flag, if not found.</b>
     *
     * @see PendingIntent
     */
    public int getFlag() {
        if (!exists()) {
            logNotExists(FLAG);
            return 0;
        }
        int flag = 0;
        try {
            if (jsonAlarmData.has(FLAG)) flag = jsonAlarmData.getInt(FLAG);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return flag;
    }

    /**
     * Returns the <b>maximal</b> time period between each alarm in milliseconds.
     * <br></br><br></br>
     * <b>Returns -1 if not found.</b>
     */
    public int getMaxDeltaPeriod() {
        if (!exists()) {
            logNotExists(MAX_DELTA_PERIOD);
            return -1;
        }
        int maxDeltaPeriod = -1;
        try {
            if (jsonAlarmData.has(MAX_DELTA_PERIOD))
                maxDeltaPeriod = jsonAlarmData.getInt(MAX_DELTA_PERIOD);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return maxDeltaPeriod;
    }

    /**
     * Returns the <b>minimal</b> time period between each alarm in milliseconds.
     * <br></br><br></br>
     * <b>Returns -1 if not found.</b>
     */
    public int getMinDeltaPeriod() {
        if (!exists()) {
            logNotExists(MIN_DELTA_PERIOD);
            return -1;
        }
        int minDeltaPeriod = -1;
        try {
            if (jsonAlarmData.has(MIN_DELTA_PERIOD))
                minDeltaPeriod = jsonAlarmData.getInt(MIN_DELTA_PERIOD);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return minDeltaPeriod;
    }

    /**
     * Returns the exact time stamp in milliseconds,as an offset from
     * the Epoch, January 1, 1970 00:00:00.000 GMT (Gregorian), until which
     * Alarms should be instantiated.
     * <br></br><br></br>
     * <b>Returns -1 if not found.</b>
     *
     * @see Alarm
     * @see java.util.Calendar
     */
    public long getLastUntilTimeInMillis() {
        if (!exists()) {
            logNotExists(LAST_UNTIL_TIME_IN_MILLIS);
            return -1;
        }
        long lastUntilTimeInMillis = -1;
        try {
            if (jsonAlarmData.has(LAST_UNTIL_TIME_IN_MILLIS))
                lastUntilTimeInMillis = jsonAlarmData.getLong(LAST_UNTIL_TIME_IN_MILLIS);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return lastUntilTimeInMillis;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            HOURS, MINUTES, TYPE,
            SHOW_LOG, AlARM_ID, RECEIVERS,
            REQUESTCODE, FLAG, MAX_DELTA_PERIOD,
            MIN_DELTA_PERIOD, LAST_UNTIL_TIME_IN_MILLIS
    })
    @interface AlarmValueKeys {
    }

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String HOURS = "hours";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String MINUTES = "minutes";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String TYPE = "type";

    /**
     * A Key for a <b>boolean<b/>
     *
     * @see Boolean
     */
    public static final String SHOW_LOG = "showLog";

    /**
     * A Key for a <b>String<b/>
     *
     * @see String
     */
    public static final String AlARM_ID = "alarmId";

    /**
     * The key for the <b>JSONArray<b/> that contains all receivers.
     *
     * @see JSONArray
     */
    public static final String RECEIVERS = "receivers";

    /**
     * The key for the simple class name of a single receiver.
     * This key unlocks a <b>String</b>.
     *
     * @see String
     */
    public static final String RECEIVER_ID = "receiver";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String REQUESTCODE = "requestCode";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String FLAG = "flag";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String MAX_DELTA_PERIOD = "maxDeltaPeriod";

    /**
     * A Key for an <b>int<b/>
     *
     * @see Integer
     */
    public static final String MIN_DELTA_PERIOD = "minDeltaPeriod";

    /**
     * A Key for a <b>long<b/>
     *
     * @see Long
     */
    public static final String LAST_UNTIL_TIME_IN_MILLIS = "lastUntilTimeInMillis";

}


