package com.u_12ampere.staywake.receiver.notification;

import android.app.Notification;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import io.flutter.Log;

import com.u_12ampere.staywake.Utils;
import com.u_12ampere.staywake.receiver.Receiver;

// Todo: finish this
public class NotificationReceiver extends Receiver {

    public static final String TITLE_KEY = getKey(NotificationReceiver.class, "_title");
    public static final String MESSAGE_KEY = getKey(NotificationReceiver.class, "_message");
    public static final String CHANNEL_ID_KEY = getKey(NotificationReceiver.class, "_channelId");
    public static final String NOTIFICATION_ID_KEY = getKey(NotificationReceiver.class, "_notificationId");

    @Override
    public void onReceive(Context context, Bundle bundle) {
        final String title = bundle.getString(TITLE_KEY);
        final String message = bundle.getString(MESSAGE_KEY);
        final String channelId = bundle.getString(CHANNEL_ID_KEY);
        final int id = bundle.getInt(NOTIFICATION_ID_KEY, 1);

        NotificationHelper.initialize(context);

        Log.i(Utils.LOG_TAG_RECEIVER, "\nchannel Id: <" + channelId
                + ">\ntitle: <" + title
                + ">\nmessage: <" + message
                + ">\n notification id: <" + id
                + ">");

        NotificationCompat.Builder nb = NotificationHelper
                .getNotification(
                        context,
                        channelId,
                        title,
                        message
                );

        nb.setVibrate(new long[]{150, 50, 100, 150, 50, 100, 150, 50, 100, 150, 50, 100,});
        nb.setLights(Color.argb(255, 150, 0, 150), 1000, 500);
        nb.setPriority(Notification.PRIORITY_MAX);
        nb.setDefaults(Notification.DEFAULT_SOUND);

        NotificationHelper.getManager().notify(id, nb.build());
    }
}
