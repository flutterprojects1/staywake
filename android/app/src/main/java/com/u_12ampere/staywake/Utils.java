package com.u_12ampere.staywake;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

public final class Utils {
    public static final String UTILS_TAG = Utils.class.getSimpleName() + "_TAG";

    public static final String LOG_TAG_RECEIVER = "Test_Receiver";

    public static final String ROOT_PACKAGE_NAME = "com.u_12ampere.staywake";

    public static boolean isOreoOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static boolean isMarshmallowOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    /**
     * Returns <b>null</b>, if <b>list</b> is empty
     */
    @Nullable
    public static <E> E getRandom(@Nullable List<E> list) {
        if (list == null) return null;
        if (list.isEmpty()) return null;
        if (list.size() == 1) return list.get(0);
        Collections.shuffle(list);
        return list.get(0);
    }

    /**
     * Generates a random Long within <b>[min;max]</b>, including
     * <b>min</b> and <b>max</b>.
     * <p>
     * If <b>min</b> is greater than, or equal to <b>max</b>, -1 gets returned.
     *
     * @param min Minimun value which can possibly be the outcome.
     *            Has to be greater than, or equal to zero.
     * @param max Maximum value which can possibly be the outcome
     *            Has to be greater than, or equal to zero.
     */
    public static long randomLong(long min, long max) {
        if (min < 0 || max < 0 || min >= max) return -1;
        return (long) (min + Math.random() * ((max - min) + 1));
    }

    /**
     * Get a millisecond value that is an offset from the
     * <b>Epoch, January 1, 1970 00:00:00.000 GMT (Gregorian)</b>.
     *
     * @param hourOfDay   The hour of day. Has to be within [0;23], else -1 gets returned.
     * @param minute      Has to be within [0;59], else -1 gets returned.
     * @param second      Has to be within [0;59], else -1 gets returned.
     * @param autoCorrect If you call <b>getTimeInMillis(15, 22, 0, true)</b>,
     *                    but the current time is <b>Sat Aug 22 19:33:41 GMT 2020</b>, then <b>autoCorrect</b>
     *                    returns the timestamp of <b>Sun Aug 23 15:22:00 GMT 2020</b>, instead of
     *                    <b>Sat Aug 22 15:22:00 GMT 2020</b> in milliseconds.
     * @see Calendar
     */
    public static long getTimeInMillis(int hourOfDay, int minute, int second, boolean autoCorrect) {
        if (0 > hourOfDay || 24 <= hourOfDay) return -1;
        if (0 > minute || 60 <= minute) return -1;
        if (0 > second || 60 <= second) return -1;

        long millis;

        if (autoCorrect) {
            long now = getCurrentTimeInMillis();
            Calendar c = Calendar.getInstance();

            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            c.set(Calendar.SECOND, second);
            millis = c.getTimeInMillis();

            if (millis <= now) {
                c.set(Calendar.HOUR_OF_DAY, hourOfDay + 24);
                millis = c.getTimeInMillis();
            }

        } else {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            c.set(Calendar.SECOND, second);
            millis = c.getTimeInMillis();
        }

        return millis;
    }


    public static long getCurrentTimeInMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }

    @NonNull
    public static String getCurrentDayOfWeek(@Nullable TimeZone timeZone) {
        Calendar c = Calendar.getInstance();
        String day = "";

        if (null != timeZone) c.setTimeZone(timeZone);

        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                day = "Montag";
                break;

            case Calendar.TUESDAY:
                day = "Dienstag";
                break;

            case Calendar.WEDNESDAY:
                day = "Mittwoch";
                break;

            case Calendar.THURSDAY:
                day = "Donnerstag";
                break;

            case Calendar.FRIDAY:
                day = "Freitag";
                break;

            case Calendar.SATURDAY:
                day = "Samstag";
                break;

            case Calendar.SUNDAY:
                day = "Sonntag";
                break;
        }

        return day;
    }

    public static void startService(Context context, Class<?> cls) {
        Intent service = new Intent(context, cls);

        if (isOreoOrAbove())
            context.startForegroundService(service);
        else
            context.startService(service);
    }

    public static void stopService(Context context, Class<?> cls) {
        context.stopService(new Intent(context, cls));
    }
}
