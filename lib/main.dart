import 'package:flutter/material.dart';

import 'backend/utils.dart';
import 'frontend/widgets/pages/application.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await loadAllAssets();

  await Application.run();
}
