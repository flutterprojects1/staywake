abstract class GolosUI {
  GolosUI._();

  static const String regular = "GolosUI Regular";

  static const String medium = "GolosUI Medium";

  static const String bold = "GolosUI Bold";

  static const String vf = "GolosUI VF";
}

abstract class Roboto {
  Roboto._();

  static const String regular = "Roboto";
}
