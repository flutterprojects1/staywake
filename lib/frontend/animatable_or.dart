import 'package:flutter/material.dart';

class AnimatableOr<T> {
  final T backup;
  final Animatable<T?>? animatable;

  const AnimatableOr({
    required this.backup,
    this.animatable,
  });

  const AnimatableOr.backup(this.backup) : animatable = null;

  bool get isAnimatable => animatable != null;

  Animation<T?>? animate(Animation<double> parent) =>
      animatable?.animate(parent);
}
