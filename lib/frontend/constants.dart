import 'package:flutter/material.dart';

const radius = Radius.circular(8);
const borderRadius = BorderRadius.all(radius);
