import 'package:flutter/material.dart';

import '../animatable_or.dart';
import '../types.dart';

class AnimatableIcon extends StatefulWidget {
  const AnimatableIcon({
    required this.notifier,
    this.icon,
    this.duration,
    this.size,
    this.color,
    this.semanticLabel,
    this.textDirection,
    Key? key,
  }) : super(key: key);

  final IconData? icon;

  /// See [Icon.semanticLabel].
  final String? semanticLabel;

  /// See [Icon.textDirection].
  final TextDirection? textDirection;

  /// This controlles what the animation should do
  final AnimationNotifier notifier;

  /// The length of time this animation should last.
  ///
  /// See [AnimationController.duration]
  final Duration? duration;

  /// The animation which is applied to [Icon.size].
  final AnimatableOr<double>? size;

  /// The animation which is applied to [Icon.color].
  final AnimatableOr<Color>? color;

  @override
  _AnimatableIconState createState() => _AnimatableIconState();
}

class _AnimatableIconState extends State<AnimatableIcon>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  Animation<double?>? sizeAnimation;
  Animation<Color?>? colorAnimation;

  double? currentValue;

  void handleUpdate() {
    switch (widget.notifier.value) {
      case AnimationMode.forward:
        controller.forward();
        break;

      case AnimationMode.reverse:
        controller.reverse();
        break;

      default:
    }
  }

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    )..addListener(() {
        currentValue = controller.value;

        if (null != widget.key)
          PageStorage.of(context)?.writeState(
            context,
            currentValue,
            identifier: widget.key,
          );
      });

    sizeAnimation = widget.size?.animatable?.animate(controller);
    colorAnimation = widget.color?.animatable?.animate(controller);

    if (null != widget.key)
      currentValue = PageStorage.of(context)?.readState(
        context,
        identifier: widget.key,
      );

    widget.notifier.addListener(handleUpdate);

    if (null != currentValue) controller.value = currentValue!;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller.view,
      builder: (context, child) {
        return Icon(
          widget.icon,
          size: sizeAnimation?.value ?? widget.size?.backup,
          color: colorAnimation?.value ?? widget.color?.backup,
          semanticLabel: widget.semanticLabel,
          textDirection: widget.textDirection,
        );
      },
    );
  }

  @override
  void dispose() {
    widget.notifier.removeListener(handleUpdate);
    controller.dispose();
    super.dispose();
  }
}
