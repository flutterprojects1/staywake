import 'package:flutter/material.dart';

class ElevatedContainer extends StatelessWidget {
  const ElevatedContainer({
    this.child,
    this.height,
    this.width,
    this.borderWidth = 20,
    this.margin,
    this.padding,
  });

  /// The [child] contained by the container.
  final Widget? child;

  /// The [height] value includes the padding.
  final double? height;

  final double? width;

  /// The [borderWidth] determines the thickness of the border.
  final double? borderWidth;

  /// Empty space to surround the [borderWidth] and [child].
  final EdgeInsetsGeometry? margin;

  /// The [child], if any, is placed inside this padding.
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    EdgeInsetsGeometry? _margin = margin;
    if (null != width) {
      final screenWidth = MediaQuery.of(context).size.width;
      _margin = EdgeInsets.only(
        left: (screenWidth - width!) / 2,
        right: (screenWidth - width!) / 2,
      );
    }

    EdgeInsetsGeometry? _insertionMargin;
    if (null != borderWidth) _insertionMargin = EdgeInsets.all(borderWidth!);

    return Container(
      margin: _margin,
      decoration: BoxDecoration(
        color: theme.accentColor,
        borderRadius: BorderRadius.circular(20),
        // boxShadow: [
        //   BoxShadow(
        //     color: theme.shadowColor,
        //     spreadRadius: 1,
        //     blurRadius: 4,
        //     offset: Offset(0, 1.5),
        //   ),
        // ],
      ),
      child: Container(
        padding: EdgeInsets.zero,
        margin: _insertionMargin,
        decoration: BoxDecoration(
          color: theme.primaryColor,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: theme.shadowColor,
              spreadRadius: 0.6,
              blurRadius: 1.6,
              offset: const Offset(3.5, 4.5),
            ),
          ],
        ),
        child: Container(
          height: height,
          alignment: Alignment.center,
          margin: EdgeInsets.zero,
          padding: padding,
          child: child,
        ),
      ),
    );
  }
}
