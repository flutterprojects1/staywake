import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../backend/alarm/alarm.dart';

class FAB extends StatelessWidget {
  const FAB({
    this.child = const Icon(Icons.arrow_forward, size: 32),
    this.size = 76,
    this.margin = const EdgeInsets.fromLTRB(0, 0, 8, 8),
    this.onPressed,
  })  : _background = null,
        _alignment = null;

  const FAB.withBackground({
    required Widget background,
    this.child = const Icon(Icons.arrow_forward, size: 32),
    this.size = 76,
    this.margin = const EdgeInsets.fromLTRB(0, 0, 20, 40),
    this.onPressed,
    Alignment alignment = Alignment.bottomRight,
  })  : _background = background,
        _alignment = alignment;

  final Widget? child;
  final double? size;
  final EdgeInsetsGeometry? margin;
  final void Function(AlarmData alarmData)? onPressed;

  final Widget? _background;
  final Alignment? _alignment;

  @override
  Widget build(BuildContext context) {
    final Widget fab = Container(
      height: size,
      width: size,
      margin: margin,
      child: FittedBox(
        child: Consumer<AlarmData>(
          builder: (_, value, __) {
            return FloatingActionButton(
              child: child,
              onPressed: () => onPressed?.call(value),
            );
          },
        ),
      ),
    );

    if (null == _background || null == _alignment) return fab;

    return Stack(children: [
      _background!,
      Align(
        alignment: _alignment!,
        child: fab,
      ),
    ]);
  }
}
