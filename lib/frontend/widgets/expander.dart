import 'package:flutter/material.dart';

import '../animatable_or.dart';

class Expander extends StatefulWidget {
  const Expander({
    required this.child,
    this.borderRadius = const AnimatableOr<BorderRadiusGeometry>.backup(
      BorderRadius.all(Radius.circular(8)),
    ),
    this.border = const AnimatableOr<Border>.backup(Border()),
    this.expandedChild,
    this.pageStorageKey,
    this.initiallyExpanded = false,
    this.expansionTime = const Duration(milliseconds: 200),
    this.onExpansionIsChanging,
    this.margin = const EdgeInsets.all(0),
    this.expansionCurve = Curves.easeIn,
    this.background = const AnimatableOr<Color>.backup(Colors.transparent),
    this.expandedChildBackground =
        const AnimatableOr<Color>.backup(Colors.transparent),
  }) : super(key: pageStorageKey);

  /// The widget which will always be displayed, if the user taps on [child]
  /// then this [Expander] expands.
  final Widget child;

  /// This widget which is displayed, beneath [child], when this [Expander] is expanded.
  final Widget? expandedChild;

  final Key? pageStorageKey;

  /// Specifies if this [Expander] is initially expanded `(true)` or collapsed `(false)`.
  final bool initiallyExpanded;

  final Duration? expansionTime;

  /// Called when this [Expander] starts expanding or collapsing.
  ///
  /// When this [Expander] starts expanding, this function is called with the value
  /// true. When this [Expander] starts collapsing, this function is called with
  /// the value false.
  final ValueChanged<bool>? onExpansionIsChanging;

  final EdgeInsetsGeometry? margin;

  /// This is the [Curve] which is used for the expansion animation.
  final Curve expansionCurve;

  /// This animatable is applied to the background of this entire
  /// [Expander].
  final AnimatableOr<Color> background;

  /// This animatable is applied to the background of [expandedChild].
  final AnimatableOr<Color> expandedChildBackground;

  /// This animatable is applied to the borderRadius of this entire
  /// [Expander].
  final AnimatableOr<BorderRadiusGeometry> borderRadius;

  /// This animatable is applied to the border of this entire
  /// [Expander].
  final AnimatableOr<Border> border;

  @override
  _ExpanderState createState() => _ExpanderState();
}

class _ExpanderState extends State<Expander>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  late Animation<double> heightFactorAnimation;
  late Animation<double> expandedChildOpacityAnimation;

  Animation<Color?>? backgroundAnimation;
  Animation<Border?>? borderAnimation;
  Animation<BorderRadiusGeometry?>? borderRadiusAnimation;
  Animation<Color?>? expandedChildBackgroundAnimation;

  late bool isExpanded;

  void handleTap() {
    isExpanded = !isExpanded;

    widget.onExpansionIsChanging?.call(isExpanded);

    if (isExpanded) {
      controller.forward();
    } else {
      controller.reverse().then((_) {
        if (!mounted) return;
        setState(() {
          // rebuild without widget.children
        });
      });
    }

    PageStorage.of(context)?.writeState(
      context,
      isExpanded,
      identifier: widget.pageStorageKey,
    );
  }

  Widget buildChildren(BuildContext context, Widget? child) {
    return GestureDetector(
      onTap: handleTap,
      child: Container(
        margin: widget.margin,
        decoration: BoxDecoration(
          color: backgroundAnimation?.value ?? widget.background.backup,
          borderRadius:
              borderRadiusAnimation?.value ?? widget.borderRadius.backup,
          border: borderAnimation?.value,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            widget.child,
            if (null != child)
              ClipRect(
                child: ColoredBox(
                  color: expandedChildBackgroundAnimation?.value ??
                      widget.expandedChildBackground.backup,
                  child: Align(
                    heightFactor: heightFactorAnimation.value,
                    child: FadeTransition(
                      opacity: expandedChildOpacityAnimation,
                      child: child,
                    ),
                  ),
                ),
              )
            else
              const SizedBox(),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: widget.expansionTime,
    );

    heightFactorAnimation = CurveTween(
      curve: Interval(0, 0.8, curve: widget.expansionCurve),
    ).animate(controller);
    expandedChildOpacityAnimation = CurveTween(
      curve: Interval(0.7, 1, curve: widget.expansionCurve),
    ).animate(controller);

    backgroundAnimation = widget.background.animatable?.animate(controller);
    borderAnimation = widget.border.animatable?.animate(controller);
    borderRadiusAnimation = widget.borderRadius.animatable?.animate(controller);
    expandedChildBackgroundAnimation =
        widget.expandedChildBackground.animatable?.animate(controller);

    isExpanded = PageStorage.of(context)?.readState(
          context,
          identifier: widget.pageStorageKey,
        ) as bool? ??
        widget.initiallyExpanded;

    if (isExpanded) controller.value = 1.0;
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !isExpanded && controller.isDismissed;

    return AnimatedBuilder(
      animation: controller.view,
      builder: buildChildren,
      child: closed ? null : widget.expandedChild,
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
