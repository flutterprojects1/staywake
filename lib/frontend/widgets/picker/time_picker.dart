import 'package:flutter/material.dart';

class TimePicker extends StatefulWidget {
  const TimePicker({
    this.height = 200,
    this.onPick,
    this.selectedColor = Colors.white,
    this.defaultColor = Colors.white,
    this.numberSize = 45.0,
    this.numberSpacing = 15.0,
    this.numberLoopFlex = 6,
    this.colonFlex = 4,
    this.colonHeight = 0.9,
    this.initialHour = 0,
    this.initialMinute = 0,
  })  : assert(
          numberLoopFlex + colonFlex == 10 &&
              numberLoopFlex % 2 == 0 &&
              colonFlex % 2 == 0,
        ),
        assert(initialHour >= 0 && initialHour < 60),
        assert(initialMinute >= 0 && initialMinute < 60);

  final double height;

  final void Function(int hours, int mins)? onPick;

  final Color? selectedColor;

  final Color defaultColor;

  final double numberSize;

  final double numberSpacing;

  final int numberLoopFlex;

  final int colonFlex;

  final double? colonHeight;

  final int initialHour;

  final int initialMinute;

  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  late int hour;
  late int min;

  @override
  void initState() {
    super.initState();
    hour = widget.initialHour;
    min = widget.initialMinute;
  }

  @override
  Widget build(BuildContext context) {
    final _style = Number.getStyle(context);
    final style = TextStyle(
      background: _style.background,
      backgroundColor: _style.backgroundColor,
      color: widget.selectedColor ?? _style.color,
      debugLabel: _style.debugLabel,
      decoration: _style.decoration,
      decorationColor: _style.decorationColor,
      decorationStyle: _style.decorationStyle,
      decorationThickness: _style.decorationThickness,
      fontFamily: _style.fontFamily,
      fontFamilyFallback: _style.fontFamilyFallback,
      fontFeatures: _style.fontFeatures,
      fontSize: widget.numberSize,
      fontStyle: _style.fontStyle,
      fontWeight: _style.fontWeight,
      foreground: _style.foreground,
      height: widget.colonHeight ?? _style.height,
      inherit: _style.inherit,
      letterSpacing: _style.letterSpacing,
      locale: _style.locale,
      shadows: _style.shadows,
      textBaseline: _style.textBaseline,
      wordSpacing: _style.wordSpacing,
    );

    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: widget.height,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  fit: FlexFit.loose,
                  flex: widget.numberLoopFlex ~/ 2,
                  child: NumberLoopList(
                    initialIndex: widget.initialHour,
                    numberSize: widget.numberSize,
                    numberSpacing: widget.numberSpacing,
                    defaultColor: widget.defaultColor,
                    selectedColor: widget.selectedColor,
                    amountOfNumbers: 24,
                    onSelectedItemChanged: (idx) {
                      hour = idx;
                      widget.onPick?.call(hour, min);
                    },
                  ),
                ),
                Flexible(
                  flex: widget.colonFlex,
                  child: Text(
                    ':',
                    style: style,
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  flex: widget.numberLoopFlex ~/ 2,
                  child: NumberLoopList(
                    initialIndex: widget.initialMinute,
                    numberSize: widget.numberSize,
                    numberSpacing: widget.numberSpacing,
                    defaultColor: widget.defaultColor,
                    selectedColor: widget.selectedColor,
                    amountOfNumbers: 60,
                    onSelectedItemChanged: (idx) {
                      min = idx;
                      widget.onPick?.call(hour, min);
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Number extends StatelessWidget {
  const Number(
    this.value, {
    this.color,
    this.size = 45.0,
  });

  final int value;
  final Color? color;
  final double? size;

  static TextStyle getStyle(BuildContext context) =>
      Theme.of(context).textTheme.headline4!;

  @override
  Widget build(BuildContext context) {
    final _style = getStyle(context);
    final style = TextStyle(
      background: _style.background,
      backgroundColor: _style.backgroundColor,
      color: color ?? _style.color,
      debugLabel: _style.debugLabel,
      decoration: _style.decoration,
      decorationColor: _style.decorationColor,
      decorationStyle: _style.decorationStyle,
      decorationThickness: _style.decorationThickness,
      fontFamily: _style.fontFamily,
      fontFamilyFallback: _style.fontFamilyFallback,
      fontFeatures: _style.fontFeatures,
      fontSize: size ?? _style.fontSize,
      fontStyle: _style.fontStyle,
      fontWeight: _style.fontWeight,
      foreground: _style.foreground,
      height: _style.height,
      inherit: _style.inherit,
      letterSpacing: _style.letterSpacing,
      locale: _style.locale,
      shadows: _style.shadows,
      textBaseline: _style.textBaseline,
      wordSpacing: _style.wordSpacing,
    );

    return Text("${value < 10 ? "0" : ""}$value", style: style);
  }
}

class NumberLoopList extends StatefulWidget {
  const NumberLoopList({
    required this.amountOfNumbers,
    this.indexOffset = 0,
    this.onSelectedItemChanged,
    this.numberSize = 45.0,
    this.numberSpacing = 15.0,
    this.defaultColor = Colors.grey,
    this.selectedColor = Colors.blue,
    this.diameterRatio = 1.5,
    this.initialIndex = 0,
  });

  /// The amount of numbers, this is equal to the [NumberLoopList]'s length
  final int amountOfNumbers;

  /// If [indexOffset] is `5`, then the first index in the [NumberLoopList]
  /// is `5` instead of `0`.
  final int indexOffset;

  final void Function(int index)? onSelectedItemChanged;

  /// This factor determines the size of the [Number] widget in this list.
  final double numberSize;

  /// [numberSpacing] determines the space between each [Number].
  final double numberSpacing;

  /// The color of a [Number] in this list.
  final Color? defaultColor;

  /// The color of the currently selected [Number] in this list.
  final Color? selectedColor;

  /// {@macro flutter.rendering.wheelList.diameterRatio}
  final double diameterRatio;

  /// The index that will be selected upon the first build
  final int? initialIndex;

  @override
  _NumberLoopListState createState() => _NumberLoopListState();
}

class _NumberLoopListState extends State<NumberLoopList> {
  late FixedExtentScrollController controller;
  late int selected;

  @override
  void initState() {
    super.initState();
    selected = widget.initialIndex ?? 0;
    controller = FixedExtentScrollController(initialItem: selected);
  }

  @override
  Widget build(BuildContext context) {
    return ListWheelScrollView.useDelegate(
      itemExtent: widget.numberSize + widget.numberSpacing,
      diameterRatio: widget.diameterRatio,
      physics: const FixedExtentScrollPhysics(),
      controller: controller,
      onSelectedItemChanged: (idx) {
        setState(() => selected = idx);
        widget.onSelectedItemChanged?.call(idx);
      },
      childDelegate: ListWheelChildBuilderLoopingListDelegate(
        childCount: widget.amountOfNumbers,
        startIndex: widget.indexOffset,
        builder: (context, idx) => Number(
          idx,
          color: idx == selected ? widget.selectedColor : widget.defaultColor,
          size: widget.numberSize,
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class ListWheelChildBuilderLoopingListDelegate extends ListWheelChildDelegate {
  /// Constructs the delegate from a concrete list of children.
  ListWheelChildBuilderLoopingListDelegate({
    required this.builder,
    this.childCount = 20,
    this.startIndex = 0,
  })  : assert(childCount > 0),
        assert(startIndex >= 0);

  final IndexedWidgetBuilder builder;
  final int childCount;
  final int startIndex;

  @override
  int? get estimatedChildCount => null;

  @override
  int trueIndexOf(int index) => index % childCount;

  @override
  Widget build(BuildContext context, int index) {
    final Widget child = builder(
      context,
      startIndex + (index % (childCount - startIndex)),
    );

    return IndexedSemantics(
      child: child,
      index: index,
    );
  }

  @override
  bool shouldRebuild(
      covariant ListWheelChildBuilderLoopingListDelegate oldDelegate) {
    return builder != oldDelegate.builder ||
        childCount != oldDelegate.childCount;
  }
}
