import 'package:flutter/material.dart';

class DatePicker extends StatefulWidget {
  const DatePicker({
    this.text = "Date selected: %dd%.%mm%.%yy%",
    this.onPick,
  });

  /// To directly use the [dateTime] put
  /// %yy% - year
  /// %mm% - month
  /// %dd% - day
  final String text;
  final void Function(
    DateTime old,
    DateTime? picked,
    bool canceled,
  )? onPick;

  DateTime get upperConstraint => DateTime.utc(DateTime.now().year + 2);
  DateTime get lowerConstraint => DateTime.utc(DateTime.now().year - 1);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  late DateTime dateTime;

  @override
  void initState() {
    super.initState();
    dateTime = DateTime.now();
  }

  String get timeIntext {
    final year = dateTime.year.toString();
    final month = dateTime.month.toString();
    final day = dateTime.day.toString();

    String data = widget.text.replaceAll("%yy%", year);
    data = data.replaceAll("%YY%", year);

    data = data.replaceAll("%mm%", month);
    data = data.replaceAll("%MM%", month);

    data = data.replaceAll("%dd%", day);
    data = data.replaceAll("%DD%", day);

    return data;
  }

  Future<bool> selectTime(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: widget.lowerConstraint,
      lastDate: widget.upperConstraint,
    );

//     print("""\n
// *DatePicker*
// Old dateTime: <${dateTime.toString()}>
// Picked dateTime: <${picked.toString()}>\n
// """);

    widget.onPick?.call(dateTime, picked, null == picked);

    if (picked != null && picked != dateTime) {
      setState(() {
        dateTime = picked;
      });
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(
        color: Colors.amber,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: <Widget>[
          Text(
            timeIntext,
            textScaleFactor: 1.5,
          ),
          // ignore: deprecated_member_use
          RaisedButton(
            child: const Text(
              "Pick",
              textScaleFactor: 1.25,
            ),
            onPressed: () {
              selectTime(context);
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            color: Colors.cyan,
            splashColor: Colors.blue[900],
          ),
        ],
      ),
    );
  }
}
