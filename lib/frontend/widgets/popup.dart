import 'package:flutter/material.dart';

class PopUp extends StatelessWidget {
  const PopUp({
    required this.child,
    this.calculatePosition,
    this.show = true,
  });

  /// The [Widget] shown by this [PopUp], if [show] is `true`.
  final Widget child;

  /// Used to calculate the global position of [child].
  /// If [calculatePosition] is `null`, then the global position
  /// of the center of [child] is used.
  final Offset Function(Offset center, Size boxSize)? calculatePosition;

  final bool show;

  @override
  Widget build(BuildContext context) {
    return AnchoredOverlay(
      showOverlay: show,
      child: const SizedBox.shrink(),
      overlayBuilder: (context, center, boxSize) {
        final position = calculatePosition?.call(center, boxSize) ?? center;
        return CenterAbout(
          position: position,
          child: child,
        );
      },
    );
  }
}

/// [Source](https://medium.com/coding-with-flutter/flutter-adding-animated-overlays-to-your-app-e0bb049eff39)
class AnchoredOverlay extends StatelessWidget {
  const AnchoredOverlay({
    required this.overlayBuilder,
    required this.child,
    this.showOverlay = true,
  });

  final Widget Function(BuildContext context, Offset center, Size boxSize)
      overlayBuilder;

  /// This Widget is shown if [showOverlay] is `false`.
  final Widget child;
  final bool showOverlay;

  @override
  Widget build(BuildContext context) {
    return OverlayBuilder(
      showOverlay: showOverlay,
      overlayBuilder: (overlayContext) {
        final box = context.findRenderObject() as RenderBox;
        final center = box.size.center(box.localToGlobal(Offset.zero));

        print("""
box.size: ${box.size}
center: $center
box.size.center(Offset.zero): ${box.size.center(Offset.zero)}
box.localToGlobal(Offset.zero): ${box.localToGlobal(Offset.zero)}
""");

        return overlayBuilder(overlayContext, center, box.size);
      },
      child: child,
    );
  }
}

/// [Source](https://medium.com/coding-with-flutter/flutter-adding-animated-overlays-to-your-app-e0bb049eff39)
class OverlayBuilder extends StatefulWidget {
  const OverlayBuilder({
    required this.child,
    required this.overlayBuilder,
    this.showOverlay = false,
  });

  final bool showOverlay;
  final Widget Function(BuildContext) overlayBuilder;
  final Widget child;

  @override
  _OverlayBuilderState createState() => _OverlayBuilderState();
}

class _OverlayBuilderState extends State<OverlayBuilder> {
  OverlayEntry? overlayEntry;

  bool isShowingOverlay() => null != overlayEntry;

  void showOverlay() {
    overlayEntry = OverlayEntry(builder: widget.overlayBuilder);
    addToOverlay(overlayEntry!);
  }

  void addToOverlay(OverlayEntry entry) {
    print('addToOverlay');
    Overlay.of(context)?.insert(entry);
  }

  void hideOverlay() {
    print('hideOverlay');
    overlayEntry?.remove();
    overlayEntry = null;
  }

  void syncWidgetAndOverlay() {
    if (isShowingOverlay() && !widget.showOverlay) {
      hideOverlay();
    } else if (!isShowingOverlay() && widget.showOverlay) {
      showOverlay();
    }
  }

  @override
  void initState() {
    super.initState();

    if (widget.showOverlay) {
      WidgetsBinding.instance?.addPostFrameCallback((_) => showOverlay());
    }
  }

  @override
  void didUpdateWidget(OverlayBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => syncWidgetAndOverlay());
  }

  @override
  void reassemble() {
    super.reassemble();
    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => syncWidgetAndOverlay());
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void dispose() {
    if (isShowingOverlay()) hideOverlay();
    super.dispose();
  }
}

/// [Source](https://medium.com/coding-with-flutter/flutter-adding-animated-overlays-to-your-app-e0bb049eff39)
class CenterAbout extends StatelessWidget {
  const CenterAbout({
    this.position,
    this.child,
  });

  final Offset? position;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: position?.dy,
      left: position?.dx,
      child: FractionalTranslation(
        translation: const Offset(-0.5, -0.5),
        child: child,
      ),
    );
  }
}
