import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../../../backend/alarm/alarm.dart';
import '../../../backend/receiver/receiver.dart';
import '../../../backend/utils.dart';
import '../elevated_container.dart';
import '../page_description.dart';
import '../page_heading.dart';

class ConfirmationPage extends StatelessWidget {
  const ConfirmationPage({
    required Key key,
    this.onVisibilityChanged,
  }) : super(key: key);

  final VisibilityChangedCallback? onVisibilityChanged;

  @override
  Widget build(BuildContext context) {
    final alarmData = Provider.of<AlarmData>(context);
    final DateTime picked = alarmData.picked!;
    final theme = Theme.of(context);

    String modes = "";
    final types = alarmData.receiverTypes;
    for (var i = 0; i < types.length; i++) {
      switch (types[i]) {
        case SoundReceiver:
          modes += "Töne";
          break;

        case VideoReceiver:
          modes += "Videos";
          break;

        case NotificationReceiver:
          modes += "Benachrichtigungen";
          break;
      }

      if (types.length > i + 2) {
        modes += ", ";
      } else if (types.length > i + 1) {
        modes += " und ";
      }
    }

    String bodyText;
    if (types.isEmpty) {
      bodyText =
          "Du musst mindestens einen Mode auswählen, welcher dich wachhalten soll.";
    } else {
      bodyText =
          "$modes werden dich bis ${picked.weekdayAsString} um ${picked.hourAndMinute} wach halten.";
    }

    return VisibilityDetector(
      key: key!,
      onVisibilityChanged: (info) => onVisibilityChanged?.call(info),
      child: ListView(children: [
        const PageHeading(heading: "Confirm"),
        const PageDescription(
            description:
                "Wenn du alles richtig eingestellt\nhast, dann klick auf den\nHaken unten rechts."),
        ElevatedContainer(
          margin: const EdgeInsets.only(left: 40, right: 40),
          padding: const EdgeInsets.all(10),
          child: Text(
            bodyText,
            textAlign: TextAlign.center,
            style: theme.textTheme.bodyText1,
          ),
        ),
      ]),
    );
  }
}
