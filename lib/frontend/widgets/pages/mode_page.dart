import 'package:flutter/material.dart';

import '../modetile.dart';
import '../page_description.dart';
import '../page_heading.dart';

class ModePage extends StatefulWidget {
  const ModePage({
    required this.children,
    Key? key,
  }) : super(key: key);

  final List<ModeTile> children;

  @override
  _ModePageState createState() => _ModePageState();
}

class _ModePageState extends State<ModePage> {
  late ScrollController controller;
  late double scrollOffset;

  @override
  void initState() {
    super.initState();
    scrollOffset = PageStorage.of(context)?.readState(
          context,
          identifier: widget.key,
        ) as double? ??
        0;

    controller = ScrollController(initialScrollOffset: scrollOffset)
      ..addListener(() {
        scrollOffset = controller.offset;
        PageStorage.of(context)?.writeState(
          context,
          scrollOffset,
          identifier: widget.key,
        );
      });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return CustomScrollView(
      controller: controller,
      slivers: [
        SliverAppBar(
          expandedHeight: 175,
          backgroundColor: theme.primaryColorLight,
          flexibleSpace: ListView(
            physics: const NeverScrollableScrollPhysics(),
            children: const [
              PageHeading(heading: "Mode"),
              PageDescription(
                description:
                    "Hier kannst du die Arten,\nmit denen du dich\nwach hältst, auswählen.",
              ),
            ],
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate.fixed([
            Container(
              decoration: BoxDecoration(
                color: theme.accentColor,
                borderRadius: BorderRadius.circular(25),
              ),
              margin: const EdgeInsets.only(left: 25, right: 25, bottom: 25),
              padding: const EdgeInsets.only(bottom: 10),
              child: Column(children: widget.children),
            ),
          ]),
        )
      ],
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
