import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:request_permission/request_permission.dart';

import '../../../backend/alarm/alarm.dart';
import '../../../backend/receiver/receiver.dart';
import '../../fonts.dart';
import 'tab_wrapper.dart';

class Application extends StatefulWidget {
  static Future<void> run() async {
    await AlarmCacheFile.instace.initialize();

    //Tries to only allow Portrait mode, if an Error occures
    //it launches anyway but with Portrait and landscape
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    runApp(const Application._());
  }

  const Application._();

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  static const MaterialColor primary = Colors.amber;
  static const Color primaryLight = Colors.white;

  @override
  void initState() {
    super.initState();

    RequestPermission.instace.requestMultipleAndroidPermissions(
      {}
        ..addAll(NotificationReceiver.permissions)
        ..addAll(SoundReceiver.permissions)
        ..addAll(VideoReceiver.permissions),
    );

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: primary,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider.value(
          value: AlarmData(
            alarmId: "Global-StayWake-AlarmId",
            showLog: true,
            maxDeltaPeriod: const Duration(minutes: 1, seconds: 45),
            minDeltaPeriod: const Duration(seconds: 45),
          ),
        )
      ],
      child: MaterialApp(
        home: const TabWrapper(),
        // home: TestPopUpPage(),
        // home: TestPage(),
        debugShowCheckedModeBanner: false,
        title: "StayWake",
        // This is the color of the app indicator,
        // when the user is viewing all opened tasks.
        color: primary,
        theme: ThemeData(
          primarySwatch: primary,
          primaryColor: primary,
          primaryColorLight: primaryLight,
          accentColor: Colors.amberAccent[100],
          scaffoldBackgroundColor: primaryLight,
          shadowColor: Colors.grey.withOpacity(0.3),
          platform: TargetPlatform.android,
          textTheme: const TextTheme(
            headline1: TextStyle(
              color: primaryLight,
              fontSize: 50,
              fontWeight: FontWeight.w700,
              letterSpacing: 7,
            ),
            headline2: TextStyle(
              color: Colors.black,
              fontSize: 35,
              fontFamily: GolosUI.vf,
            ),
            headline3: TextStyle(
              color: Color.fromRGBO(250, 233, 185, 1),
              fontSize: 24,
              fontFamily: GolosUI.vf,
              fontWeight: FontWeight.w600,
            ),
            headline4: TextStyle(
              color: Colors.black54,
              fontFamily: Roboto.regular,
              decoration: TextDecoration.none,
            ),
            subtitle1: TextStyle(
              color: Colors.black,
              fontSize: 22,
              fontFamily: GolosUI.vf,
            ),
            bodyText1: TextStyle(
              color: primaryLight,
              fontSize: 20,
              fontFamily: GolosUI.vf,
              letterSpacing: -0.4,
            ),
          ),
          tabBarTheme: const TabBarTheme(
            labelColor: Colors.black,
            unselectedLabelColor: Colors.black,
            indicator: UnderlineTabIndicator(
              insets: EdgeInsets.zero,
              borderSide: BorderSide(
                width: 2.5,
                color: primaryLight,
              ),
            ),
          ),
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: primary,
            foregroundColor: primaryLight,
          ),
          sliderTheme: SliderThemeData(
              activeTrackColor: const Color.fromRGBO(255, 240, 196, 1),
              inactiveTrackColor: primary,
              thumbColor: const Color.fromRGBO(255, 240, 196, 1),
              overlayColor:
                  const Color.fromRGBO(255, 240, 196, 1).withOpacity(0.12),
              activeTickMarkColor: primary,
              inactiveTickMarkColor: primaryLight,
              tickMarkShape: const RoundSliderTickMarkShape(
                tickMarkRadius: 3,
              )),
        ),
      ),
    );
  }
}
