import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../../../backend/alarm/alarm.dart';
import '../../../backend/receiver/receiver.dart';
import '../../../backend/utils.dart';
import '../fab.dart';
import '../modetile.dart';
import '../settings/settings.dart';
import 'confirmation_page.dart';
import 'mode_page.dart';
import 'time_page.dart';

class TabWrapper extends StatefulWidget {
  const TabWrapper();

  @override
  _TabWrapperState createState() => _TabWrapperState();
}

class _TabWrapperState extends State<TabWrapper> with TickerProviderStateMixin {
  late TabController tabController;
  late AnimationController fabController;

  late Animation<double> fabSizeAnim;

  void onTileStatusChange(bool isExpanded, AlarmData data,
          List<Receiver?> receivers, Type type) =>
      isExpanded ? data.addAll(receivers) : data.removeType(type);

  bool addOrRemoveReceivers<T extends Receiver>({
    required AlarmData alarmData,
    required List<T?> receivers,
    required int updatedLength,
    required T? Function(int index) onAdd,
    T? Function(int index)? onRemove,
  }) {
    int actualLength = receivers.indexWhere((element) => null == element);
    if (-1 == actualLength) actualLength = receivers.length;

    // no changed need to be made
    if (updatedLength == actualLength) return false;

    alarmData.removeType(T);

    if (actualLength > updatedLength) {
      // remove elems
      for (var i = actualLength - 1; i >= updatedLength; i--)
        receivers[i] = onRemove?.call(i);
    } else if (actualLength < updatedLength) {
      // add elements
      for (var i = actualLength; i < updatedLength; i++)
        receivers[i] = onAdd(i);
    }

    alarmData.addAll(receivers);

    return true;
  }

  @override
  void initState() {
    super.initState();

    VisibilityDetectorController.instance.updateInterval =
        const Duration(milliseconds: 100);

    tabController = TabController(
      vsync: this,
      length: 3,
    );

    fabController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 175),
    );

    fabSizeAnim = Tween<double>(begin: 0, end: 76)
        .chain(CurveTween(curve: Curves.easeIn))
        .animate(fabController);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final alarmData = Provider.of<AlarmData>(context);

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 56,
        bottom: TabBar(
          indicator: theme.tabBarTheme.indicator,
          controller: tabController,
          tabs: const [
            Tab(child: Icon(Icons.alarm)),
            Tab(child: Icon(Icons.list)),
            Tab(child: Icon(Icons.check_circle_outline)),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          TimePage(
            hourKey: const Key("TimePage.hour"),
            minuteKey: const Key("TimePage.minute"),
            onPick: (hour, min, data) {
              data.pick(hour: hour, minute: min);
            },
          ),
          ModePage(
            key: const Key("ModePage"),
            children: [
              ModeTile<SoundReceiver>(
                imagePath: "assets/modes/png-colored/wave_rainbow.png",
                onStatusChanged: onTileStatusChange,
                receivers: List<SoundReceiver?>.generate(
                  soundAssets.length,
                  (index) {
                    if (0 == index)
                      return SoundReceiver(
                        path: soundAssets.first.path,
                        closeWhenCompleted: false,
                      );
                    else
                      return null;
                  },
                ),
                settings: [
                  Heading("Sound", 1),
                  Heading("Frequency", 2),
                  ValueSlider(
                    initial: 1,
                    max: (soundAssets.loadedFiles.length - 1).toDouble(),
                    onChangeEnd: (updated, old, receivers) {
                      if (null == receivers) return false;

                      final List<String> unused = soundAssets.loadedFilesAsList
                          .map((e) => e.path)
                          .where((element) =>
                              null ==
                              receivers.firstWhere(
                                (rec) => rec?.path == element,
                                orElse: () => null,
                              ))
                          .toList()
                            ..shuffle();

                      return addOrRemoveReceivers<SoundReceiver>(
                        alarmData: alarmData,
                        receivers: receivers,
                        updatedLength: updated.truncate() + 1,
                        onAdd: (index) {
                          return SoundReceiver(
                            path: unused.removeAt(0),
                            durationFactor: receivers.first!.durationFactor,
                            closeWhenCompleted:
                                receivers.first!.closeWhenCompleted,
                          );
                        },
                      );
                    },
                  ),
                  Heading("Duration", 2),
                  ValueSlider(
                    max: 1,
                    initial: 1,
                    onChangeEnd: (updated, old, receivers) {
                      if (null == receivers) return false;
                      receivers.forEach((e) => e?.durationFactor = updated);
                      return true;
                    },
                  ),
                ],
              ),
              ModeTile<VideoReceiver>(
                imagePath: "assets/modes/png-colored/play-button_rainbow.png",
                onStatusChanged: onTileStatusChange,
                receivers: List<VideoReceiver?>.generate(
                  videoAssets.length,
                  (index) {
                    if (0 == index)
                      return VideoReceiver(
                        path: videoAssets.first.path,
                        closeWhenCompleted: false,
                      );
                    else
                      return null;
                  },
                ),
                settings: [
                  Heading("Watch", 1),
                  Heading("Frequency", 2),
                  ValueSlider(
                    initial: 1,
                    max: (videoAssets.length - 1).toDouble(),
                    onChangeEnd: (updated, old, receivers) {
                      if (null == receivers) return false;

                      final List<String> unused = videoAssets.loadedFilesAsList
                          .map((e) => e.path)
                          .where((element) =>
                              null ==
                              receivers.firstWhere(
                                (rec) => rec?.path == element,
                                orElse: () => null,
                              ))
                          .toList()
                            ..shuffle();

                      return addOrRemoveReceivers<VideoReceiver>(
                        alarmData: alarmData,
                        receivers: receivers,
                        updatedLength: updated.truncate() + 1,
                        onAdd: (index) {
                          return VideoReceiver(
                            path: unused.removeAt(0),
                            durationFactor: receivers.first!.durationFactor,
                            closeWhenCompleted:
                                receivers.first!.closeWhenCompleted,
                          );
                        },
                      );
                    },
                  ),
                  Heading("Duration", 2),
                  ValueSlider(
                    max: 1,
                    initial: 1,
                    onChangeEnd: (updated, old, receivers) {
                      if (null == receivers) return false;
                      receivers.forEach((e) => e?.durationFactor = updated);
                      return true;
                    },
                  ),
                ],
              ),
              // Todo: Modetile for NotificationReceiver
              // ModeTile<NotificationReceiver>(
              //   imagePath: "assets/modes/lamp.png",
              //   onStatusChanged: onTileStatusChange,
              //   receivers: [NotificationReceiver()],
              //   settings: [
              //     Heading("Reminder", 1),
              //     Heading("Intensitiy", 2),
              //     ValueSlider(max: 10),
              //     Heading("Brightness", 2),
              //     ValueSlider(max: 10)
              //   ],
              // ),
            ],
          ),
          ConfirmationPage(
            key: const Key("ConfirmationPage"),
            onVisibilityChanged: (info) {
              if (!fabController.isAnimating) {
                if (0.2 <= info.visibleFraction) {
                  fabController.forward();
                } else if (0.3 >= info.visibleFraction) {
                  fabController.reverse();
                }
              }
            },
          ),
        ],
      ),
      floatingActionButton: AnimatedBuilder(
          animation: fabController.view,
          child: const Icon(Icons.check, size: 32),
          builder: (context, child) {
            if (alarmData.receivers.isEmpty) return const SizedBox();

            return FAB(
              size: fabSizeAnim.value,
              child: child,
              onPressed: (_) {
                print(alarmData);

                AlarmCacheFile.instace.alarmData = alarmData;
                AlarmCacheFile.instace.write();

                Alarm.startChainReaction(
                  alarmData.receivers.getRandom()!,
                  alarmData,
                );
              },
            );
          }),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    fabController.dispose();
    super.dispose();
  }
}
