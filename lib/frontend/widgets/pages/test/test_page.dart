//@dart = 2.11

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../backend/alarm/alarm.dart';
import '../../../../backend/receiver/receiver.dart';
import '../../../../backend/utils.dart';

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  MethodChannel channel;
  AlarmCacheFile file;

  @override
  void initState() {
    super.initState();
    channel = const MethodChannel(Alarm.channeldId);
    file = AlarmCacheFile.instace;
    file.alarmData = AlarmData(
      alarmId: "sieger",
      receivers: [
        SoundReceiver(path: "lololol---testpath.mp3"),
        VideoReceiver(path: "testVidpath.mp4", durationFactor: 0.75),
        SoundReceiver(path: "path.mp3", durationFactor: 0.2),
        VideoReceiver(path: "other video.mp4", durationFactor: 0.1235),
        SoundReceiver(path: "penis just los.mp3", durationFactor: 0.2),
      ],
    )..pick(hour: 15, minute: 04);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const Text(
                  "AlarmCacheFile",
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                    child: const Text("write"),
                    onPressed: () {
                      file.write();
                    }),
                // ignore: deprecated_member_use
                RaisedButton(
                    child: const Text("log"),
                    onPressed: () {
                      channel.invokeMethod("print_cache");
                    }),
                // ignore: deprecated_member_use
                RaisedButton(
                    child: const Text("reload"),
                    onPressed: () {
                      channel.invokeMethod("reload_cache");
                    }),
                // ignore: deprecated_member_use
                RaisedButton(
                    child: const Text("jsonProperties"),
                    onPressed: () {
                      print("\n\n\n");
                      print(file.jsonProperties);
                    }),
                // ignore: deprecated_member_use
                RaisedButton(
                    child: const Text("delete AlarmCacheFile"),
                    onPressed: () {
                      print("Flutter time in millis: <$getTimeInMillis()>");
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TestAlarm extends StatefulWidget {
  @override
  _TestAlarmState createState() => _TestAlarmState();
}

class _TestAlarmState extends State<TestAlarm> {
  TimeOfDay timeOfDay = TimeOfDay.now();
  Alarm alarm;

  @override
  void initState() {
    super.initState();
    alarm = Alarm(alarmId: "lolol");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // TimePicker(
              //   onPick: (old, picked, canceled) {
              //     if (!canceled) {
              //       alarm.pick(hours: picked.hour, minutes: picked.minute);
              //     } else
              //       print("Time pick canceled");
              //   },
              // ),
              const SizedBox(height: 60),
              // ignore: deprecated_member_use
              RaisedButton(
                  child: const Text("start"),
                  onPressed: () {
                    alarm.start();
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // ignore: deprecated_member_use
                  RaisedButton(
                    child: const Text("setSoundReceiver"),
                    onPressed: () {
                      alarm.setReceiver(
                        SoundReceiver(
                            path: soundAssets.getFile("test.mp3").path),
                      );
                    },
                  ),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("setNotificationReceiver"),
                      onPressed: () {
                        alarm.setReceiver(NotificationReceiver(
                          title: "Der Title millis: Sieg heil",
                          message:
                              "Die message sei: ${alarm.hours}:${alarm.minutes}",
                        ));
                      }),
                ],
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                child: const Text("setVideoReceiver"),
                onPressed: () {
                  alarm.setReceiver(
                    VideoReceiver(
                      path: videoAssets.getFile("a.mp4").path,
                      durationFactor: 0.7,
                      closeWhenCompleted: false,
                    ),
                  );
                },
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                  child: const Text("pick"),
                  onPressed: () {
                    showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                    ).then((value) {
                      alarm.pick(hours: value.hour, minutes: value.minute);
                    });
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class TestAllFunctions extends StatefulWidget {
  @override
  _TestAllFunctionsState createState() => _TestAllFunctionsState();
}

class _TestAllFunctionsState extends State<TestAllFunctions> {
  MethodChannel channel;
  AlarmCacheFile file;
  AlarmData alarmData;
  Alarm alarm;

  @override
  void initState() {
    super.initState();
    channel = const MethodChannel(Alarm.channeldId);
    file = AlarmCacheFile.instace;
    alarmData = AlarmData(
        alarmId: "siegerData",
        maxDeltaPeriod: const Duration(minutes: 2, seconds: 10),
        minDeltaPeriod: const Duration(minutes: 1),
        showLog: true,
        flag: AlarmFlag.cancelCurrent);
    alarmData
      ..add(
        SoundReceiver(path: soundAssets.getFile("test.mp3").path),
      )
      ..add(
        VideoReceiver(
          path: videoAssets.getFile("1.mp4").path,
          durationFactor: 0.7,
          closeWhenCompleted: false,
        ),
      )
      ..add(
        NotificationReceiver(
            title: "Titel vom AlarmData NotificationR",
            message: "Die message sei: I like turtles"),
      );

    alarm = Alarm(alarmId: "lolol_SINGLE_Alarm");
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("start single"),
                      onPressed: () {
                        alarm.start();
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("pick single"),
                      onPressed: () {
                        showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.now(),
                        ).then((value) {
                          alarm.pick(hours: value.hour, minutes: value.minute);
                        });
                      })
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.amber,
                    child: const Text("setSoundR single"),
                    onPressed: () {
                      alarm.setReceiver(
                          SoundReceiver(
                              path: soundAssets.getFile("test.mp3").path),
                          requestCode: 100,
                          flag: AlarmFlag.cancelCurrent);
                    },
                  ),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("setNotificationR single"),
                      onPressed: () {
                        alarm.setReceiver(
                            NotificationReceiver(
                              title: "Der Title millis: Sieg heil",
                              message:
                                  "Die message sei: ${alarm.hours}:${alarm.minutes}",
                            ),
                            requestCode: 100,
                            flag: AlarmFlag.cancelCurrent);
                      }),
                ],
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                color: Colors.amber,
                child: const Text("setVideoR single"),
                onPressed: () {
                  alarm.setReceiver(
                      VideoReceiver(
                        path: videoAssets.getFile("1.mp4").path,
                        durationFactor: 0.5,
                        closeWhenCompleted: false,
                      ),
                      requestCode: 100,
                      flag: AlarmFlag.cancelCurrent);
                },
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("Write cache"),
                      onPressed: () {
                        file.write();
                        print("invoked file.write(), exists: ${file.exists}");
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("Delete cache"),
                      onPressed: () {
                        final bool success = file.delete();
                        print(
                            "Deletion of ${AlarmCacheFile.fileName} was ${success ? "succesful" : "a failure"} !");
                      })
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("file.jsonProperties"),
                      onPressed: () {
                        print("\n\n\n");
                        print(file.jsonProperties);
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                    child: const Text("print cache"),
                    onPressed: () {
                      channel.invokeMethod("print_cache");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("pick"),
                      onPressed: () {
                        showTimePicker(
                                context: context, initialTime: TimeOfDay.now())
                            .then((value) {
                          if (value != null) {
                            alarmData.pick(
                                hour: value.hour, minute: value.minute);
                            print("pick, ${value.hour}:${value.minute}");
                          }
                        });
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("set AlarmData"),
                      onPressed: () {
                        file.alarmData = alarmData;
                        print("set alarmData\n$alarmData");
                      }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("data.toString()"),
                      onPressed: () {
                        print(alarmData);
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("print CacheDir()"),
                      onPressed: () {
                        channel.invokeMethod("print_CacheDir");
                      }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("reload cache"),
                      onPressed: () {
                        channel.invokeMethod("reload_cache");
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("print lines file dart"),
                      onPressed: () {
                        file.alarmDataFile.readAsLines().then((value) {
                          print("""
                    AlarmDataCache file read from dart as lines:

-----------------------------------------

$value

-----------------------------------------
                    
                    
                    """);
                        });
                      }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(child: const Text(""), onPressed: () {}),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("print string file dart"),
                      onPressed: () {
                        file.alarmDataFile.readAsString().then((value) {
                          print("""
                    AlarmDataCache file read from dart as String:

-----------------------------------------

$value

-----------------------------------------
                    
                    
                    """);
                        });
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TestFinishedProduct extends StatefulWidget {
  @override
  _TestFinishedProductState createState() => _TestFinishedProductState();
}

class _TestFinishedProductState extends State<TestFinishedProduct> {
  MethodChannel channel;
  AlarmCacheFile file;
  AlarmData alarmData;
  Alarm alarm;

  @override
  void initState() {
    super.initState();
    channel = const MethodChannel(Alarm.channeldId);
    file = AlarmCacheFile.instace;
    alarmData = AlarmData(
        alarmId: "siegerData",
        maxDeltaPeriod: const Duration(minutes: 2, seconds: 10),
        minDeltaPeriod: const Duration(minutes: 1),
        showLog: true,
        flag: AlarmFlag.cancelCurrent);
    alarmData
      ..add(
        SoundReceiver(path: soundAssets.getFile("test.mp3").path),
      )
      ..add(
        VideoReceiver(
          path: videoAssets.getFile("1.mp4").path,
          durationFactor: 0.8,
          closeWhenCompleted: false,
        ),
      )
      ..add(
        NotificationReceiver(
            title: "Titel vom AlarmData NotificationR",
            message: "Die message sei: I like turtles"),
      );

    alarm = Alarm(alarmId: "lolol_SINGLE_Alarm");
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("start single"),
                      onPressed: () {
                        alarm.start();
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("pick single"),
                      onPressed: () {
                        showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.now(),
                        ).then((value) {
                          alarm.pick(hours: value.hour, minutes: value.minute);
                        });
                      })
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.amber,
                    child: const Text("setSoundR single"),
                    onPressed: () {
                      alarm.setReceiver(
                          SoundReceiver(
                              path: soundAssets.getFile("test.mp3").path),
                          requestCode: 100,
                          flag: AlarmFlag.cancelCurrent);
                    },
                  ),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.amber,
                      child: const Text("setNotificationR single"),
                      onPressed: () {
                        alarm.setReceiver(
                            NotificationReceiver(
                              title: "Der Title millis: Sieg heil",
                              message: "Die message sei: ANALROSE",
                            ),
                            requestCode: 100,
                            flag: AlarmFlag.cancelCurrent);
                      }),
                ],
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                color: Colors.amber,
                child: const Text("setVideoR single"),
                onPressed: () {
                  alarm.setReceiver(
                      VideoReceiver(
                        path: videoAssets.getFile("1.mp4").path,
                        durationFactor: 0.75,
                        closeWhenCompleted: false,
                      ),
                      requestCode: 100,
                      flag: AlarmFlag.cancelCurrent);
                },
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("Write cache.json"),
                      onPressed: () {
                        file.write();
                        print("invoked file.write(), exists: ${file.exists}");
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("Delete cache.json"),
                      onPressed: () {
                        final success = file.delete();
                        print(
                            "Deletion of ${AlarmCacheFile.fileName} was ${success ? "succesful" : "a failure"} !");
                      })
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("print CacheDir()"),
                      onPressed: () {
                        channel.invokeMethod("print_CacheDir");
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                    child: const Text("print cache.json"),
                    onPressed: () {
                      channel.invokeMethod("print_cache");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("pick last until"),
                      onPressed: () {
                        showTimePicker(
                                context: context, initialTime: TimeOfDay.now())
                            .then((value) {
                          if (value != null) {
                            alarmData.pick(
                                hour: value.hour, minute: value.minute);
                            print("pick, ${value.hour}:${value.minute}");
                            file.alarmData = alarmData;
                          }
                        });
                      }),
                  const SizedBox(width: 10),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      child: const Text("reload cache.json"),
                      onPressed: () {
                        channel.invokeMethod("reload_cache");
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
