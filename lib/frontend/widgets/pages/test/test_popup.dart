import 'package:flutter/material.dart';

import '../../popup.dart';

class TestPopUpPage extends StatefulWidget {
  @override
  _TestPopUpPageState createState() => _TestPopUpPageState();
}

class _TestPopUpPageState extends State<TestPopUpPage>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> popupAnim;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 750),
    );

    popupAnim = Tween<double>(begin: 0, end: 1).animate(controller);

    Future.delayed(const Duration(seconds: 3), () {
      controller.repeat(reverse: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            width: size.width,
            height: size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PopUp(
                  child: ScaleTransition(
                    scale: popupAnim,
                    child: ElevatedButton(
                      child: const Text("MyButton"),
                      onPressed: () {
                        print("Pressed myButton");
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      // floatingActionButton: _buildFab(context),
    );
  }
}
