import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../backend/alarm/alarm.dart';
import '../elevated_container.dart';
import '../page_description.dart';
import '../page_heading.dart';
import '../picker/time_picker.dart';

class TimePage extends StatefulWidget {
  const TimePage({
    this.onPick,
    this.hourKey,
    this.minuteKey,
  });

  /// onPick is called whenever the hour or minute of
  /// this [TimePage] changes.
  final void Function(int hour, int minute, AlarmData alarmData)? onPick;
  final Key? hourKey;
  final Key? minuteKey;

  @override
  _TimePageState createState() => _TimePageState();
}

class _TimePageState extends State<TimePage> {
  late int hour;
  late int minute;
  late bool hasUsedAlarmData;
  Key? hasUsedAlarmDataKey;
  late bool firstBuild;

  void storeTime() {
    PageStorage.of(context)?.writeState(
      context,
      hour,
      identifier: widget.hourKey,
    );
    PageStorage.of(context)?.writeState(
      context,
      minute,
      identifier: widget.minuteKey,
    );
  }

  @override
  void initState() {
    super.initState();

    firstBuild = true;

    hasUsedAlarmDataKey =
        Key("${widget.hourKey?.hashCode}+${widget.minuteKey?.hashCode}");

    hour = PageStorage.of(context)?.readState(
          context,
          identifier: widget.hourKey,
        ) as int? ??
        0;

    minute = PageStorage.of(context)?.readState(
          context,
          identifier: widget.minuteKey,
        ) as int? ??
        0;

    hasUsedAlarmData = PageStorage.of(context)?.readState(
          context,
          identifier: hasUsedAlarmDataKey,
        ) as bool? ??
        false;

    if (!hasUsedAlarmData && 0 == hour && 0 == minute) {
      AlarmCacheFile.instace.read();
      if (null != AlarmCacheFile.instace.storedAlarmData) {
        hour =
            AlarmCacheFile.instace.storedAlarmData?.lastUntilTimeOfDay?.hour ??
                0;
        minute = AlarmCacheFile
                .instace.storedAlarmData?.lastUntilTimeOfDay?.minute ??
            0;

        PageStorage.of(context)?.writeState(
          context,
          true,
          identifier: hasUsedAlarmDataKey,
        );

        storeTime();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final AlarmData alarmData = Provider.of<AlarmData>(context);

    if (firstBuild) widget.onPick?.call(hour, minute, alarmData);
    firstBuild = false;

    return ListView(
      children: [
        const PageHeading(heading: "Time"),
        const PageDescription(
          description: "Bis wann sollst du\nwach gehalten werden?",
        ),
        ElevatedContainer(
          margin: const EdgeInsets.only(left: 40, right: 40),
          padding: const EdgeInsets.only(top: 20, bottom: 20),
          child: TimePicker(
            initialHour: hour,
            initialMinute: minute,
            height: 170,
            colonFlex: 2,
            numberLoopFlex: 8,
            onPick: (hours, mins) {
              hour = hours;
              minute = mins;

              storeTime();

              widget.onPick?.call(hour, minute, alarmData);
            },
          ),
        ),
      ],
    );
  }
}
