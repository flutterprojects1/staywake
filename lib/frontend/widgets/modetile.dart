import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../backend/alarm/alarm.dart';
import '../../backend/receiver/receiver.dart';
import '../../backend/utils.dart';
import '../animatable_or.dart';
import '../constants.dart';
import '../types.dart';
import 'animatable_icon.dart';
import 'expander.dart';
import 'settings/settings.dart';
import 'sinecurve.dart';

typedef StatusChangeCallback<T extends Receiver> = void Function(
    bool isExpanded,
    AlarmData alarmData,
    List<T?> receivers,
    Type receiverType);

class ModeTile<T extends Receiver> extends StatefulWidget {
  const ModeTile({
    required this.settings,
    required this.imagePath,
    required this.receivers,
    this.cardHeight = 130,
    this.onStatusChanged,
    this.margin = const EdgeInsets.fromLTRB(10, 0, 10, 0),
    this.animationDuration = const Duration(milliseconds: 380),
  });

  final List<SettingsBase<T>> settings;
  final String imagePath;
  final List<T?> receivers;
  final StatusChangeCallback<T>? onStatusChanged;
  final double cardHeight;
  final EdgeInsetsGeometry margin;
  final Duration animationDuration;

  @override
  _ModeTileState<T> createState() => _ModeTileState<T>();
}

class _ModeTileState<T extends Receiver> extends State<ModeTile<T>>
    with SingleTickerProviderStateMixin {
  late bool isExpanded;
  late AnimationNotifier animationsNotifier;
  late List<Widget> settingsWidgets;
  late List<T?> receivers;

  late Key expanderKey;
  late Key iconKey;
  late Key receiversKey;

  Key getKey(String signature) =>
      Key("ModeTile-$signature:${widget.imagePath.hashCode}");

  /// This function stores data for the receivers.
  void store() => PageStorage.of(context)?.writeState(
        context,
        receivers,
        identifier: receiversKey,
      );

  @override
  void initState() {
    super.initState();
    isExpanded = false;

    animationsNotifier = AnimationNotifier(AnimationMode.none);

    expanderKey = getKey("ExpanderKey");
    iconKey = getKey("IconKey");
    receiversKey = getKey("ReceiversKey");

    receivers = PageStorage.of(context)?.readState(
          context,
          identifier: receiversKey,
        ) as List<T?>? ??
        widget.receivers;

    settingsWidgets = [];
    for (var i = 0; i < widget.settings.length; i++) {
      widget.settings[i].receivers = receivers;
      widget.settings[i].store = store;
      widget.settings[i].key = getKey("$i.SettingKey-${i.hashCode}");
      settingsWidgets.add(widget.settings[i].widget);
    }
  }

  @override
  Widget build(BuildContext context) {
    final AlarmData alarmData = Provider.of<AlarmData>(context);
    final theme = Theme.of(context);

    return Container(
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        boxShadow: [
          BoxShadow(
            color: theme.shadowColor,
            spreadRadius: 0.6,
            blurRadius: 1.6,
            offset: const Offset(3.5, 4.5),
          ),
        ],
      ),
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 10),
      child: Expander(
        pageStorageKey: expanderKey,
        expansionTime: Duration(
          milliseconds: (widget.animationDuration.inMilliseconds * 4) ~/ 3,
        ),
        expandedChild: Column(children: settingsWidgets),
        expandedChildBackground: AnimatableOr<Color>.backup(theme.primaryColor),
        onExpansionIsChanging: (expanded) {
          setState(() => isExpanded = expanded);

          animationsNotifier.value =
              expanded ? AnimationMode.forward : AnimationMode.reverse;

          widget.onStatusChanged?.call(expanded, alarmData, receivers, T);
        },
        child: _Tile(
          key: iconKey,
          animationDuration: widget.animationDuration,
          animationsNotifier: animationsNotifier,
          cardHeight: widget.cardHeight,
          imagePath: widget.imagePath,
          cardWidth: getWindowWidth(context: context, percent: 1) -
              widget.margin.horizontal -
              40,
        ),
      ),
    );
  }
}

class _Tile extends StatelessWidget {
  const _Tile({
    required this.animationsNotifier,
    required this.imagePath,
    this.cardHeight,
    this.cardWidth,
    this.animationDuration,
    Key? key,
  }) : super(key: key);

  final double? cardHeight;
  final double? cardWidth;
  final String imagePath;
  final Duration? animationDuration;
  final AnimationNotifier animationsNotifier;

  @override
  Widget build(BuildContext context) {
    final windowWidth = getWindowWidth(context: context, percent: 1);
    final theme = Theme.of(context);

    return Container(
      height: cardHeight,
      width: windowWidth,
      color: theme.primaryColorLight,
      child: Stack(
        // Todo: check if overflow and clipBehavior do the same
        // overflow: Overflow.clip,
        clipBehavior: Clip.hardEdge,
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
              height: cardHeight,
              width: cardWidth,
              padding: const EdgeInsets.only(top: 10, bottom: 4),
              child: Image.asset(
                imagePath,
                fit: BoxFit.contain,
                excludeFromSemantics: true,
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: AnimatableIcon(
              key: key,
              icon: Icons.check_circle,
              notifier: animationsNotifier,
              duration: animationDuration,
              size: const AnimatableOr.backup(50),
              color: AnimatableOr(
                animatable: ColorTween(
                  begin: theme.primaryColorLight,
                  end: theme.primaryColor,
                ),
                backup: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 30,
            left: 0,
            height: cardHeight,
            width: cardWidth,
            child: AnimatableSineCurve(
              key: key,
              notifier: animationsNotifier,
              amplitude: AnimatableOr(
                animatable: Tween<double>(begin: 15, end: 30),
                backup: SinePainter.kAmplitude,
              ),
              phaseShift: const AnimatableOr.backup(3.9),
              angularFrequency: const AnimatableOr.backup(
                SinePainter.kAngularFrequency,
              ),
              offset: const AnimatableOr.backup(SinePainter.kOffset),
              color: AnimatableOr.backup(theme.primaryColor),
              deltaX: const AnimatableOr.backup(SinePainter.kDeltaX),
            ),
          ),
        ],
      ),
    );
  }
}
