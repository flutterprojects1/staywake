import 'dart:math' as math;

import 'package:flutter/material.dart';

import '../animatable_or.dart';
import '../types.dart';

class AnimatableSineCurve extends StatefulWidget {
  const AnimatableSineCurve({
    required this.notifier,
    this.animationDuration = const Duration(milliseconds: 250),
    this.colorFillMode = SinePainter.kFillMode,
    this.angularFrequency = const AnimatableOr<double>.backup(
      SinePainter.kAngularFrequency,
    ),
    this.amplitude = const AnimatableOr<double>.backup(
      SinePainter.kAmplitude,
    ),
    this.offset = const AnimatableOr<double>.backup(
      SinePainter.kOffset,
    ),
    this.phaseShift = const AnimatableOr<double>.backup(
      SinePainter.kPhaseShift,
    ),
    this.color = const AnimatableOr<Color>.backup(
      SinePainter.kFillColor,
    ),
    this.deltaX = const AnimatableOr<double>.backup(
      SinePainter.kDeltaX,
    ),
    Key? key,
  }) : super(key: key);

  /// This controlles what the animation should do.
  final AnimationNotifier notifier;

  final Duration? animationDuration;

  final FillMode colorFillMode;

  /// The animation which is applied to [SinePainter.angularFrequency].
  final AnimatableOr<double> angularFrequency;

  /// The animation which is applied to [SinePainter.amplitude].
  final AnimatableOr<double> amplitude;

  /// The animation which is applied to [SinePainter.offset].
  final AnimatableOr<double> offset;

  /// The animation which is applied to [SinePainter.phaseShift].
  final AnimatableOr<double> phaseShift;

  /// The animation which is applied to [SinePainter.color].
  final AnimatableOr<Color> color;

  /// The animation which is applied to [SinePainter.deltaX].
  final AnimatableOr<double> deltaX;

  @override
  _AnimatableSineCurveState createState() => _AnimatableSineCurveState();
}

class _AnimatableSineCurveState extends State<AnimatableSineCurve>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  Animation<double?>? angularFrequency;
  Animation<double?>? amplitude;
  Animation<double?>? offset;
  Animation<double?>? phaseShift;
  Animation<Color?>? color;
  Animation<double?>? deltaX;

  void handleUpdate() {
    switch (widget.notifier.value) {
      case AnimationMode.forward:
        controller.forward();
        break;

      case AnimationMode.reverse:
        controller.reverse();
        break;

      default:
        break;
    }
  }

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: widget.animationDuration,
    )..addListener(() {
        if (null != widget.key)
          PageStorage.of(context)?.writeState(
            context,
            controller.value,
            identifier: widget.key,
          );
      });

    angularFrequency = widget.angularFrequency.animate(controller);
    amplitude = widget.amplitude.animate(controller);
    offset = widget.offset.animate(controller);
    phaseShift = widget.phaseShift.animate(controller);
    color = widget.color.animate(controller);
    deltaX = widget.deltaX.animate(controller);

    widget.notifier.addListener(handleUpdate);

    if (null != widget.key)
      controller.value = PageStorage.of(context)?.readState(
            context,
            identifier: widget.key,
          ) as double? ??
          0;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller.view,
      builder: (context, child) {
        return CustomPaint(
          painter: SinePainter(
            repaint: controller.view,
            precision: 2,
            fillMode: widget.colorFillMode,
            angularFrequency:
                angularFrequency?.value ?? widget.angularFrequency.backup,
            amplitude: amplitude?.value ?? widget.amplitude.backup,
            offset: offset?.value ?? widget.offset.backup,
            phaseShift: phaseShift?.value ?? widget.phaseShift.backup,
            color: color?.value ?? widget.color.backup,
            deltaX: deltaX?.value ?? widget.deltaX.backup,
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    widget.notifier.removeListener(handleUpdate);
    controller.dispose();
    super.dispose();
  }
}

class SinePainter extends CustomPainter {
  static const double kAngularFrequency = 0.023;
  static const double kAmplitude = 40;
  static const double kOffset = 0;
  static const double kPhaseShift = math.pi;
  static const Color kFillColor = Colors.amber;
  static const FillMode kFillMode = FillMode.under;
  static const double kDeltaX = 10;

  const SinePainter({
    this.angularFrequency = kAngularFrequency,
    this.amplitude = kAmplitude,
    this.offset = kOffset,
    this.phaseShift = kPhaseShift,
    this.color = kFillColor,
    this.fillMode = kFillMode,
    this.deltaX = kDeltaX,
    this.precision = 1,
    Listenable? repaint,
  }) : super(repaint: repaint);

  /// Graphically, this causes an `extension or
  /// compression` in the `x direction` with the factor
  /// `1/[angularFrequency]`
  final double angularFrequency;

  /// Graphically, this causes an `extension or
  /// compression` in the `y direction`.
  ///
  ///
  /// If `null` then the available height will be used.
  final double amplitude;

  /// The number `[offset]` is added to each function value.
  /// Graphically, this causes a `shift parallel to the y-axis` by
  /// `[offset]`
  final double offset;

  /// The additive constant [phaseShift] in the argument
  /// causes a phase shift. Graphically, this causes a
  /// shift on the x-axis by `[phaseShift]/[angularFrequency]`
  ///
  ///
  /// `[phaseShift] > 0` then to the `left`
  ///
  /// `[phaseShift] < 0` then to the `right`
  final double phaseShift;

  final Color? color;

  final FillMode fillMode;

  /// [deltaX] affects in which interval the sine function will be drawn
  /// in the following way: `[deltaX; windowWidth+deltaX].
  ///
  ///
  /// It does not change anything if you give `+10` or `-10`
  /// as it's value, because the absolute value will be used
  /// when determining the interval.
  final double deltaX;

  /// [precision] determines how much differcence,
  /// for the x argument, there is between each point
  /// of the sine curve.
  ///
  /// The lower this value is, the more accurate the sinecurve will become,
  /// but setting this value `too low introduces immense jank`.
  /// Therefore it is not exactly recommended to set it lower
  /// than its default value.
  final double precision;

  static final Paint _sinePaint = Paint()
    ..strokeWidth = 10
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..isAntiAlias = true
    ..color = Colors.green[400]!;

  static final Paint _fillSinePaint = Paint()
    ..strokeWidth = 6
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..isAntiAlias = true
    ..color = Colors.green[400]!;

  double _sin(double x) =>
      amplitude * math.sin(angularFrequency * x + phaseShift) + offset;

  bool equals(Object other) {
    if (identical(this, other)) return true;

    if (other is! SinePainter) return false;

    if (other.angularFrequency != angularFrequency) return false;
    if (other.amplitude != amplitude) return false;
    if (other.offset != offset) return false;
    if (other.phaseShift != phaseShift) return false;
    if (other.color != color) return false;
    if (other.fillMode != fillMode) return false;
    if (other.deltaX != deltaX) return false;

    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;

    canvas.translate(0, height / 2);

    final Path sinPath = Path();
    for (double x = -deltaX.abs(); x < width + deltaX.abs(); x += precision)
      sinPath.lineTo(x, _sin(x));

    canvas.drawPath(
      sinPath,
      Paint()
        ..strokeWidth = _sinePaint.strokeWidth
        ..strokeCap = _sinePaint.strokeCap
        ..style = _sinePaint.style
        ..isAntiAlias = _sinePaint.isAntiAlias
        ..color = color ?? _sinePaint.color,
    );

    if (FillMode.none != fillMode) {
      final Path fillUnderSinPath = Path();
      late double groundPointY;

      switch (fillMode) {
        case FillMode.over:
          groundPointY = -height / 2;
          break;

        case FillMode.under:
          groundPointY = height / 2;
          break;

        default:
          break;
      }

      for (double x = -deltaX.abs(); x < width + deltaX.abs(); x += 6) {
        fillUnderSinPath
          ..moveTo(x, groundPointY)
          ..lineTo(x, _sin(x));
      }

      canvas.drawPath(
        fillUnderSinPath,
        Paint()
          ..strokeWidth = _fillSinePaint.strokeWidth
          ..strokeCap = _fillSinePaint.strokeCap
          ..style = _fillSinePaint.style
          ..isAntiAlias = _fillSinePaint.isAntiAlias
          ..color = color ?? _fillSinePaint.color,
      );
    }
  }

  @override
  bool shouldRepaint(SinePainter oldDelegate) => !equals(oldDelegate);
}

enum FillMode { over, under, none }
