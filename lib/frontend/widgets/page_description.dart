import 'package:flutter/material.dart';

class PageDescription extends StatelessWidget {
  const PageDescription({
    required this.description,
    this.margin = const EdgeInsets.only(top: 30, bottom: 20),
    this.padding,
  });

  final String description;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      margin: margin,
      padding: padding,
      child: Text(
        description,
        softWrap: true,
        textAlign: TextAlign.center,
        style: theme.textTheme.subtitle1,
      ),
    );
  }
}
