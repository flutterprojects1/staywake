import 'package:flutter/material.dart';

class PageHeading extends StatelessWidget {
  const PageHeading({
    required this.heading,
    this.margin,
    this.padding = const EdgeInsets.only(top: 10),
  });

  final String heading;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      margin: margin,
      padding: padding,
      child: Text(
        heading,
        softWrap: true,
        textAlign: TextAlign.center,
        style: theme.textTheme.headline2,
      ),
    );
  }
}
