import 'package:flutter/material.dart';

import '../../../../backend/receiver/receiver.dart';
import 'settingsbase.dart';

class Heading<T extends Receiver> extends SettingsBase<T> {
  Heading(
    this.title,
    this.level,
  );

  final String title;
  final int level;

  @override
  Widget get widget => _HeadingWidget(this);
}

class _HeadingWidget extends StatelessWidget {
  const _HeadingWidget(this.heading);

  final Heading heading;

  @override
  Widget build(BuildContext context) {
    Widget headingWidget;
    switch (heading.level) {
      case 1:
        headingWidget = _Heading1._(heading.title);
        break;

      case 2:
        headingWidget = _Heading2._(heading.title);
        break;

      default:
        headingWidget = _Heading1._(heading.title);
    }
    return headingWidget;
  }
}

class _Heading1 extends StatelessWidget {
  const _Heading1._(this.title);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 7),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline1,
      ),
    );
  }
}

class _Heading2 extends StatelessWidget {
  const _Heading2._(this.title);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline3,
      ),
    );
  }
}
