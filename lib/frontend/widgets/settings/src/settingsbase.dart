import 'package:flutter/material.dart';

import '../../../../backend/receiver/receiver.dart';

abstract class SettingsBase<T extends Receiver> {
  SettingsBase();

  List<T?>? receivers;

  Widget get widget;

  Key? key;

  /// Store any changed data from the receivers in the ModeTile
  /// that is associated with these settings.
  VoidCallback? store;
}
