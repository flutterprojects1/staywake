import 'package:flutter/material.dart';

import '../../../../backend/receiver/receiver.dart';
import 'settingsbase.dart';

typedef OnChangeSliderValue<T extends Receiver> = bool Function(
  double updated,
  double old,
  List<T?>? receivers,
);

class ValueSlider<T extends Receiver> extends SettingsBase<T> {
  ValueSlider({
    this.initial = 0.0,
    this.max = 1.0,
    this.min = 0.0,
    this.divisions,
    this.onChange,
    this.onChangeStart,
    this.onChangeEnd,
  });

  final double initial;
  final double max;
  final double min;

  /// The number of discrete divisions.
  ///
  /// If null, the slider is continuous.
  final int? divisions;

  /// Called during a drag when the user is selecting a new value for the slider
  /// by dragging.
  ///
  /// The slider passes the new value to the callback but does not actually
  /// change state until the parent widget rebuilds the slider with the new
  /// value.
  ///
  /// If this function returns `true`, then any changes made
  /// to [receivers] is saved in the [PageStorage],
  /// else nothing happens.
  final OnChangeSliderValue<T>? onChange;

  /// Called when the user starts selecting a new value for the slider.
  ///
  /// This callback notifies you when the user has started
  /// selecting a new value by starting a drag or with a tap.
  ///
  /// The value passed will be the last value that the
  /// slider had before the change began.
  ///
  /// If this function returns `true`, then any changes made
  /// to [receivers] is saved in the [PageStorage],
  /// else nothing happens.
  final OnChangeSliderValue<T>? onChangeStart;

  /// Called when the user is done selecting a new value for the slider.
  ///
  /// This callback notifies you when the user has completed
  /// selecting a new [value] by ending a drag or a click.
  ///
  /// If this function returns `true`, then any changes made
  /// to [receivers] is saved in the [PageStorage],
  /// else nothing happens.
  final OnChangeSliderValue<T>? onChangeEnd;

  @override
  Widget get widget => _ValueSliderWidget<T>(this);
}

class _ValueSliderWidget<T extends Receiver> extends StatefulWidget {
  const _ValueSliderWidget(this.valueSlider);

  final ValueSlider<T> valueSlider;

  @override
  _ValueSliderWidgetState<T> createState() => _ValueSliderWidgetState<T>();
}

class _ValueSliderWidgetState<T extends Receiver>
    extends State<_ValueSliderWidget<T>> {
  late double currentValue;

  void callOnChange(OnChangeSliderValue<T>? callback, double value) {
    if (callback?.call(value, currentValue, widget.valueSlider.receivers) ??
        false) widget.valueSlider.store?.call();
  }

  @override
  void initState() {
    super.initState();

    currentValue = PageStorage.of(context)?.readState(
          context,
          identifier: widget.valueSlider.key,
        ) as double? ??
        widget.valueSlider.initial;
  }

  @override
  Widget build(BuildContext context) {
    return Slider(
      max: widget.valueSlider.max,
      min: widget.valueSlider.min,
      value: currentValue,
      divisions: widget.valueSlider.divisions,
      onChanged: (value) {
        callOnChange(widget.valueSlider.onChange, value);
        setState(() => currentValue = value);
      },
      onChangeStart: (value) {
        callOnChange(widget.valueSlider.onChangeStart, value);
      },
      onChangeEnd: (value) {
        callOnChange(widget.valueSlider.onChangeEnd, value);

        PageStorage.of(context)?.writeState(
          context,
          currentValue,
          identifier: widget.valueSlider.key,
        );
      },
    );
  }
}
