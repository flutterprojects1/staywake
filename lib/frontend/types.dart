import 'package:flutter/foundation.dart';

class AnimationNotifier = ValueNotifier<AnimationMode> with ToAlias;

enum AnimationMode {
  none,
  forward,
  reverse,
}

mixin ToAlias {}
