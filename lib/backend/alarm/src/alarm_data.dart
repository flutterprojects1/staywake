import 'package:flutter/material.dart';

import '../../receiver/receiver.dart';
import '../../utils.dart';
import 'alarm.dart';

class AlarmData {
  TimeOfDay? _lastUntilTimeOfDay;
  int? _lastUntilTimeInMillis;
  late AlarmType type;
  late bool showLog;
  String alarmId;
  late int _requestCode;
  late AlarmFlag flag;

  late Duration _maxDeltaPeriod;
  late Duration _minDeltaPeriod;

  late List<Receiver?> _receivers;

  AlarmData({
    required this.alarmId,
    TimeOfDay? lastUntilTimeOfDay,
    int? lastUntilTimeInMillis,
    this.type = AlarmType.rtcWakeup,
    this.showLog = false,
    int requestCode = 1,
    this.flag = AlarmFlag.updateCurrent,
    Duration maxDeltaPeriod = const Duration(minutes: 5),
    Duration minDeltaPeriod = const Duration(minutes: 3),
    List<Receiver?>? receivers,
  }) {
    if (null != lastUntilTimeOfDay?.hour)
      assert(lastUntilTimeOfDay!.hour >= 0 && lastUntilTimeOfDay.hour <= 23);

    if (null != lastUntilTimeOfDay?.minute)
      assert(
          lastUntilTimeOfDay!.minute >= 0 && lastUntilTimeOfDay.minute <= 59);

    if (null != lastUntilTimeInMillis) assert(0 <= lastUntilTimeInMillis);

    assert(requestCode >= 0);
    assert(maxDeltaPeriod > minDeltaPeriod);

    _lastUntilTimeOfDay = lastUntilTimeOfDay;
    _lastUntilTimeInMillis = lastUntilTimeInMillis;
    _requestCode = requestCode;
    _maxDeltaPeriod = maxDeltaPeriod;
    _minDeltaPeriod = minDeltaPeriod;
    _receivers = receivers ?? [];
  }

  TimeOfDay? get lastUntilTimeOfDay => _lastUntilTimeOfDay;
  int? get lastUntilTimeInMillis => _lastUntilTimeInMillis;
  int get requestCode => _requestCode;
  Duration get maxDeltaPeriod => _maxDeltaPeriod;
  Duration get minDeltaPeriod => _minDeltaPeriod;
  List<Receiver?> get receivers => _receivers;
  DateTime? get picked => (null != lastUntilTimeInMillis)
      ? DateTime.fromMillisecondsSinceEpoch(lastUntilTimeInMillis!)
      : null;

  bool get hasValidValues => !isEmpty && (maxDeltaPeriod > minDeltaPeriod);

  bool get isEmpty =>
      !hasDataForAlarm || (receivers.isEmpty || null == lastUntilTimeInMillis);

  bool get hasDataForAlarm => !(null == _lastUntilTimeOfDay?.hour ||
      null == _lastUntilTimeOfDay?.minute);

  List<Type> get receiverTypes {
    final List<Type> types = [];
    receivers.forEach((element) {
      if (!types.contains(element.runtimeType)) types.add(element.runtimeType);
    });
    return types;
  }

  List<Map<String, Object>> get _receiversAsJSONArray {
    final List<Map<String, Object>> array = [];
    for (final receiver in _receivers)
      if (null != receiver) array.add(receiver.toMap());
    return array;
  }

  void pick({required int hour, required int minute}) {
    assert(hour >= 0 && hour <= 23);
    assert(minute >= 0 && minute <= 59);

    _lastUntilTimeOfDay = TimeOfDay(hour: hour, minute: minute);
    _lastUntilTimeInMillis = getTimeInMillis(hourOfDay: hour, minute: minute);
  }

  set requestCode(int requestCode) {
    assert(requestCode >= 0);
    _requestCode = requestCode;
  }

  set maxDeltaPeriod(Duration maxDeltaPeriod) {
    assert(maxDeltaPeriod > minDeltaPeriod);
    _maxDeltaPeriod = maxDeltaPeriod;
  }

  set minDeltaPeriod(Duration minDeltaPeriod) {
    assert(maxDeltaPeriod > minDeltaPeriod);
    _minDeltaPeriod = minDeltaPeriod;
  }

  void add(Receiver? receiver) {
    if (null != receiver) _receivers.add(receiver);
  }

  void addAll(List<Receiver?> receivers) {
    for (final item in receivers) add(item);
  }

  Receiver? removeAt(int index) => (index >= 0 && index <= _receivers.length)
      ? _receivers.removeAt(index)
      : null;

  bool remove(Receiver receiver) => _receivers.remove(receiver);

  void removeAll(List<Receiver> receivers) {
    for (final item in receivers) remove(item);
  }

  /// Remove all Receivers from [receivers] that satisfy [test].
  void removeWhere(bool Function(Receiver? receiver) test) {
    receivers.removeWhere((element) => test(element));
  }

  /// Removes every element from [receivers]
  /// that has a runTimeType of [type].
  void removeType(Type type) => removeWhere((e) => e.runtimeType == type);

  Map<String, Object> toMap() => hasValidValues
      ? {
          "hours": lastUntilTimeOfDay!.hour,
          "minutes": lastUntilTimeOfDay!.minute,
          "lastUntilTimeInMillis": lastUntilTimeInMillis!,
          "type": type.nativeValue,
          "showLog": showLog,
          "alarmId": alarmId,
          "requestCode": requestCode,
          "flag": flag.nativeValue,
          "maxDeltaPeriod": maxDeltaPeriod.inMilliseconds,
          "minDeltaPeriod": minDeltaPeriod.inMilliseconds,
          "receivers": _receiversAsJSONArray,
        }
      : {};

  @override
  String toString() {
    return """
******************************* AlarmData *******************************
hasValidValues: $hasValidValues
hasDataForAlarm: $hasDataForAlarm
isEmpty: $isEmpty

lastUntilTimeOfDay: $lastUntilTimeOfDay
lastUntilTimeInMillis: $lastUntilTimeInMillis
type: $type
showLog: $showLog
alarmId: $alarmId
requestCode: $requestCode
flag: $flag
maxDeltaPeriod: $maxDeltaPeriod
minDeltaPeriod: $minDeltaPeriod
picked: $picked
receivers.length: ${receivers.length}
receivers: $receivers
""";
  }
}
