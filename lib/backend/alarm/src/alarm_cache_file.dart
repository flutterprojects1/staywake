import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import '../../receiver/receiver.dart';
import '../alarm.dart';
import 'alarm.dart';
import 'alarm_data.dart';

class AlarmCacheFile {
  static const String fileName = "AlarmDataCache.json";

  static AlarmCacheFile? _instace;

  // ignore: prefer_constructors_over_static_methods
  static AlarmCacheFile get instace => _instace ??= AlarmCacheFile._();

  File? _alarmDataFile;
  AlarmData? alarmData;
  AlarmData? _storedAlarmData;

  AlarmCacheFile._();

  File? get alarmDataFile => _alarmDataFile;
  AlarmData? get storedAlarmData => _storedAlarmData;
  bool get isInitialized => null != alarmDataFile;
  bool get hasValidAlarmData => alarmData?.hasValidValues ?? false;
  bool get exists => alarmDataFile?.existsSync() ?? false;
  String get jsonProperties => json.encode(alarmData?.toMap());

  /// * `An error gets thrown when the first accessed method or
  ///   field of the [AlarmCacheFile.instace] is anything other
  ///   than [initialize].`
  ///
  ///
  /// * Use this function to force the initialization
  ///   of the global [AlarmCacheFile.instace].
  ///
  ///
  /// * Nothing happens if [AlarmCacheFile.instace]
  ///   has already been initialized.
  Future<AlarmCacheFile> initialize() async {
    if (!isInitialized)
      await getTemporaryDirectory()
          .then((value) => _alarmDataFile = File("${value.path}/$fileName"));

    return this;
  }

  /// * Reads data from the AlarmCacheFile in the local storage,
  ///   and places it in [AlarmCacheFile.storedAlarmData].
  ///
  ///
  /// * If no AlarmCacheFile currently exists, then nothing happens.
  ///   and [AlarmCacheFile.storedAlarmData] is null.
  ///
  ///
  /// * Returns `true` if there was a AlarmCacheFile in the devices
  ///   storage that has successfully been read, else `false`.
  bool read() {
    if (!exists) return false;

    final cachedAlarmData = jsonDecode(_alarmDataFile!.readAsStringSync());

    // AlarmType.rtcWakeup is a temporary assignment.
    AlarmType type = AlarmType.rtcWakeup;
    type = type.from(cachedAlarmData["type"] as int)!;

    // AlarmFlag.updateCurrent is a temporary assignment.
    AlarmFlag flag = AlarmFlag.updateCurrent;
    flag = flag.from(cachedAlarmData["flag"] as int)!;

    final List<Receiver> receivers =
        Receiver.parseList(cachedAlarmData["receivers"] as List);

    _storedAlarmData = AlarmData(
      lastUntilTimeOfDay: TimeOfDay(
        hour: cachedAlarmData["hours"] as int,
        minute: cachedAlarmData["minutes"] as int,
      ),
      lastUntilTimeInMillis: cachedAlarmData["lastUntilTimeInMillis"] as int,
      type: type,
      showLog: cachedAlarmData["showLog"] as bool,
      alarmId: cachedAlarmData["alarmId"] as String,
      requestCode: cachedAlarmData["requestCode"] as int,
      flag: flag,
      maxDeltaPeriod: Duration(
        milliseconds: cachedAlarmData["maxDeltaPeriod"] as int,
      ),
      minDeltaPeriod: Duration(
        milliseconds: cachedAlarmData["minDeltaPeriod"] as int,
      ),
      receivers: receivers,
    );

    return true;
  }

  bool write() {
    if (!hasValidAlarmData) return false;
    if (null == alarmDataFile) return false;

    _alarmDataFile!.writeAsStringSync(
      jsonProperties,
      mode: FileMode.writeOnly,
    );

    return true;
  }

  /// * This method deletes the `AlarmCacheFile.json` from the local storage,
  ///   which effectively ends all future Alarms.
  ///
  ///
  /// * Returns `true`, if the deletion was successful.
  bool delete([void Function(Object error)? onError]) {
    if (!exists) return false;

    try {
      _alarmDataFile!.deleteSync();
    } catch (e) {
      onError?.call(e);
      return false;
    }

    return true;
  }

  @override
  String toString() {
    return """
***************************** AlarmCacheFile *****************************
isInitialized: $isInitialized
hasValidAlarmData: $hasValidAlarmData
exists: ${isInitialized ? exists : null}
alarmDataFile: $alarmDataFile
alarmData: $alarmData
""";
  }
}
