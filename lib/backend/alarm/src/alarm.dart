import 'package:flutter/services.dart';

import '../../receiver/receiver.dart';
import '../../utils.dart';
import 'alarm_cache_file.dart';
import 'alarm_data.dart';

class Alarm {
  static const String channeldId = "$rootPackage/staywake";
  static const MethodChannel _channel = MethodChannel(channeldId);

  static const String nativeMethodId = "Alarm:";

  static set setGlobalShowLog(bool gloablShowLog) => _channel.invokeMethod(
      "MethodHandler_setGlobalShowLog", {"gloablShowLog": gloablShowLog});

  static Future<bool?> getGlobalShowLog() =>
      _channel.invokeMethod<bool>("MethodHandler_getGlobalShowLog");

  /// ## Description
  ///
  /// * Use this to induce a chain reaction resulting in
  ///   continues [Alarm]'s being fired, until
  ///   [AlarmData.lastUntilTimeOfDay] is reached.
  ///
  ///
  /// * If [receiver] is `null`, [AlarmData.hasDataForAlarm]
  ///   is `false`, [AlarmCacheFile.hasValidAlarmData] is `false`,
  ///   or [AlarmCacheFile.exists] is `false`,
  ///   then `false gets returned and nothing happens`.
  ///
  ///
  /// ## Returns
  ///
  /// * Wether starting the [Alarm] for the chain reaction was
  ///   successful or not.
  static Future<bool> startChainReaction(
    Receiver receiver,
    AlarmData alarmData,
  ) async {
    if (!alarmData.hasDataForAlarm)
      return Future.error("Alarmdata does not have enought data for an Alarm.");

    await AlarmCacheFile.instace.initialize();

    if (!AlarmCacheFile.instace.exists)
      return Future.error("AlarmCacheFile does not exits!");

    if (!AlarmCacheFile.instace.hasValidAlarmData)
      return Future.error("AlarmCacheFile does not have valid AlarmData!");

    final alarm = Alarm(
      alarmId: alarmData.alarmId,
      showLog: alarmData.showLog,
      type: alarmData.type,
    );

    await alarm.setReceiver(
      receiver,
      flag: alarmData.flag,
      requestCode: alarmData.requestCode,
    );

    // Pick a time for this Alarm,
    // because this [alarm] is the one which induces
    // the chain reaction for all other alarms.
    final randomTime = getRandomTimeOfDay(
      max: alarmData.maxDeltaPeriod,
      min: alarmData.minDeltaPeriod,
    );

    await alarm.pick(
      hours: randomTime.hour,
      minutes: randomTime.minute,
    );

    await alarm._invoke("startChainReaction");

    return true;
  }

  /// Every Alarm has it's own unique identification
  final String alarmId;
  int? _hours;
  int? _minutes;
  late AlarmType _type;
  late bool _showLog;

  /// Every instance of an Alarm has to have an id, null is not acceptable
  Alarm({
    required this.alarmId,
    AlarmType type = AlarmType.rtcWakeup,
    bool showLog = true,
  }) {
    _type = type;
    _showLog = showLog;

    _invoke("constructor", {
      "type": _type.nativeValue,
      "showLog": _showLog,
    });
  }

  int? get hours => _hours;
  int? get minutes => _minutes;
  AlarmType get type => _type;
  bool get showLog => _showLog;

  Future<T?> _invoke<T>(String method, [Map<String, Object>? args]) {
    if (null == args) {
      return _channel.invokeMethod(
        nativeMethodId + method,
        {"alarmId": alarmId},
      );
    } else {
      args.addAll({"alarmId": alarmId});
      return _channel.invokeMethod(
        nativeMethodId + method,
        args,
      );
    }
  }

  Future<bool?> get hasAction => _invoke("isHasAction");

  Future<bool?> get hasStarted => _invoke("isHasStarted");

  Future<Alarm> setShowLog(bool showLog) async {
    _showLog = showLog;
    await _invoke("setShowLog", {"showLog": showLog});
    return this;
  }

  /// ## Parameters
  ///
  /// * [receiver] - The receiver that executes an action when the alarm fires.
  ///
  ///
  /// * [requestCode] - The requestCode for the android `PendingIntent`.
  ///   Every Alarm must have a different [requestCode]
  ///
  ///
  /// * [flags] - The flags for the android `PendingIntend`, may be one of the following
  ///   [AlarmFlag.cancelCurrent], [AlarmFlag.immutable],
  ///   [AlarmFlag.noCreate], [AlarmFlag.oneShot]
  ///   [AlarmFlag.updateCurrent] or [AlarmFlag.none] if you do not
  ///   want to pass a flag.
  Future<Alarm> setReceiver(
    Receiver receiver, {
    int requestCode = 1,
    AlarmFlag flag = AlarmFlag.updateCurrent,
  }) async {
    await _channel.invokeMethod(receiver.nativeMethodId, receiver.args);
    await _invoke("setAction", {
      "requestCode": requestCode,
      "flags": flag.nativeValue,
    });

    return this;
  }

  Future<Alarm> setType(AlarmType type) async {
    _type = type;
    await _invoke("setType", {"type": type.nativeValue});
    return this;
  }

  Future<Alarm> pick({required int hours, required int minutes}) async {
    _hours = hours;
    _minutes = minutes;

    // await _invoke("pick", {"hours": hours, "minutes": minutes});
    await pickExact(getTimeInMillis(hourOfDay: hours, minute: minutes));

    return this;
  }

  /// Pick a [timePoint] for this alarm to fire. The [timePoint] is
  /// an offset from the `Epoch, January 1, 1970 00:00:00.000 GMT (Gregorian)`
  /// in milliseconds.
  Future<Alarm> pickExact(int timePoint) async {
    final time = DateTime.fromMillisecondsSinceEpoch(timePoint);
    _hours = time.hour;
    _minutes = time.minute;
    await _invoke("pickExact", {"timePoint": timePoint});
    return this;
  }

  void start() => _invoke("start");

  void cancel() => _invoke("cancel");

  void dispose() => _invoke("dispose");

  @override
  String toString() {
    return """
********************************* Alarm *********************************
alarmId: $alarmId
hours: $hours
minutes: $minutes
type: $type
showLog: $showLog
""";
  }
}

enum AlarmType {
  rtc,
  rtcWakeup,
  elapsedRealtimeWakeup,
  elapsedRealtime,
}

enum AlarmFlag {
  cancelCurrent,
  immutable,
  noCreate,
  oneShot,
  updateCurrent,
  none,
}

extension TypeNativeValue on AlarmType {
  int get nativeValue {
    switch (this) {
      case AlarmType.rtc:
        return 0;

      case AlarmType.rtcWakeup:
        return 1;

      case AlarmType.elapsedRealtimeWakeup:
        return 2;

      case AlarmType.elapsedRealtime:
        return 3;
    }
  }

  AlarmType? from(int nativeValue) {
    switch (nativeValue) {
      case 0:
        return AlarmType.rtc;

      case 1:
        return AlarmType.rtcWakeup;

      case 2:
        return AlarmType.elapsedRealtimeWakeup;

      case 3:
        return AlarmType.elapsedRealtime;
    }

    return null;
  }
}

/// Corresponds to androids PendingIntent flags
extension FlagNativeValue on AlarmFlag {
  int get nativeValue {
    switch (this) {
      case AlarmFlag.cancelCurrent:
        return 268435456;

      case AlarmFlag.immutable:
        return 67108864;

      case AlarmFlag.noCreate:
        return 536870912;

      case AlarmFlag.oneShot:
        return 1073741824;

      case AlarmFlag.updateCurrent:
        return 134217728;

      case AlarmFlag.none:
        return 0;
    }
  }

  AlarmFlag? from(int nativeValue) {
    switch (nativeValue) {
      case 268435456:
        return AlarmFlag.cancelCurrent;

      case 67108864:
        return AlarmFlag.immutable;

      case 536870912:
        return AlarmFlag.noCreate;

      case 1073741824:
        return AlarmFlag.oneShot;

      case 134217728:
        return AlarmFlag.updateCurrent;

      case 0:
        return AlarmFlag.none;
    }

    return null;
  }
}
