import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class Assets {
  /// ## Description
  ///
  /// * This is the path inside your assets folder where your files lie.
  ///
  ///
  /// * For example you have a file in the your assets directory
  ///   in flutter, let's call it
  ///   ```
  ///   var fileName = 'assets/pics/beautyful.jpg';
  ///   ```
  ///   if you want to use that file then [prefix] should be
  ///   `'pics'` note that the `'/'` should not be included
  final String prefix;

  /// How your `assets` folder is called
  final String assets;

  final Map<String, File> _loadedFiles;

  Assets({
    this.prefix = "",
    this.assets = "assets",
  })  : assert(assets.isNotEmpty),
        _loadedFiles = {};

  /// A reference to the loaded files.
  Map<String, File> get loadedFiles => _loadedFiles;

  List<File> get loadedFilesAsList =>
      _loadedFiles.values.toList(growable: false);

  /// Returns the amount of elements loaded in the cache of this [Assets] Object.
  int get length => loadedFiles.length;

  /// Returns the first element from the cache of this [Assets] Object.
  File get first => this[0];

  /// Returns the last element from the cache of this [Assets] Object.
  File get last => this[length - 1];

  /// Returns the `prefix` with a trailing `/`, if there is
  /// no `prefix`, an empty String gets returned.
  String get _finishedPrefix => prefix.isNotEmpty ? "$prefix/" : "";

  /// Returns the file at [index].
  File operator [](int index) => loadedFilesAsList[index];

  /// Remove [fileName] from the loaded cache.
  ///
  /// Does nothing if the file was not on cache.
  File? remove(String fileName) =>
      _loadedFiles.remove(_finishedPrefix + fileName);

  /// Clears the whole cache.
  void clearCache() => _loadedFiles.clear();

  Future<ByteData> _fetchAsset(String fileName) async =>
      rootBundle.load("$assets/$_finishedPrefix$fileName");

  Future<File> fetchToMemory(String fileName) async {
    final tmpDir = await getTemporaryDirectory();
    final file = File('${tmpDir.path}/$_finishedPrefix$fileName');

    // If the [file] already exists, then there is no need to create and override it
    if (file.existsSync()) return file;

    file.createSync(recursive: true);

    return file.writeAsBytes(
      (await _fetchAsset(fileName)).buffer.asUint8List(),
    );
  }

  /// Loads all the [fileNames] provided to the cache.
  Future<Map<String, File>> loadAll(List<String> fileNames) async {
    for (final fileName in fileNames) await load(fileName);
    return _loadedFiles;
  }

  /// Loads a single [fileName] to the cache.
  ///
  /// Also returns a [Future] to access that file.
  Future<File?> load(String fileName) async {
    if (!_loadedFiles.containsKey(_finishedPrefix + fileName))
      _loadedFiles[_finishedPrefix + fileName] = await fetchToMemory(fileName);

    return _loadedFiles[_finishedPrefix + fileName];
  }

  String getFileNameWithOutPrefix(String fileName) =>
      fileName.substring(_finishedPrefix.length);

  File? getFile(String fileName) => loadedFiles[_finishedPrefix + fileName];
}
