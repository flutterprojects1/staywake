import 'package:flutter/foundation.dart';

import 'receiver.dart';

class SoundReceiver extends Receiver {
  SoundReceiver({
    required this.path,
    this.durationFactor = 1,
    this.closeWhenCompleted = true,
  });

  @protected
  SoundReceiver.parser()
      : path = "",
        durationFactor = 0,
        closeWhenCompleted = false;

  /// The path to your `sound.mp3`,
  String path;

  /// This determines if the full length of the sound will be played.
  ///
  /// 1   ... full length
  ///
  /// 0.5 ... half length
  ///
  /// 0   ... nothing
  double durationFactor;

  /// If this is `true` then [durationFactor] will be ignored
  /// and the sound will be stopped when it is completed
  bool closeWhenCompleted;

  @override
  Map<String, Object> get args => {
        getKey("path"): path,
        getKey("durationFactor"): durationFactor,
        getKey("closeWhenCompleted"): closeWhenCompleted,
      };

  /// All android permissions this Receiver needs in order to
  /// work properly.
  static Set<String> get permissions => {};

  @override
  String toString() =>
      "{$simpleClassName: [path: $path, durationFactor: $durationFactor, closeWhenCompleted: $closeWhenCompleted]}";

  @override
  SoundReceiver? parseListEntry(String key, dynamic entry) {
    if (simpleClassName != key) return null;

    return SoundReceiver(
      path: entry[getKey("path")] as String,
      durationFactor: entry[getKey("durationFactor")] as double,
      closeWhenCompleted: entry[getKey("closeWhenCompleted")] as bool,
    );
  }
}
