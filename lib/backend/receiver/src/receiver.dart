import 'package:flutter/foundation.dart';

import '../../utils.dart';
import 'notification_receiver.dart';
import 'sound_receiver.dart';
import 'video_receiver.dart';

abstract class Receiver {
  static const String _receiverId = "receiver";

  const Receiver();

  String get nativeMethodId => "$rootPackage.$simpleClassName";

  String get simpleClassName => runtimeType.toString();

  Map<String, Object> get args;

  @protected
  String getKey(String subfix) => nativeMethodId + subfix;

  Map<String, Object> toMap() {
    final receiverMap = args..putIfAbsent(_receiverId, () => simpleClassName);
    return receiverMap;
  }

  @override
  String toString();

  @protected
  Receiver? parseListEntry(String key, dynamic entry);

  static List<Receiver> parseList(List entries) {
    final List<Receiver> receivers = [];

    final NotificationReceiver notificationParser = NotificationReceiver();
    final SoundReceiver soundParser = SoundReceiver.parser();
    final VideoReceiver videoParser = VideoReceiver.parser();

    entries.forEach((entry) {
      Receiver? receiver;

      final String key = entry[_receiverId];
      entry.remove(_receiverId);

      receiver = notificationParser.parseListEntry(key, entry);
      receiver ??= soundParser.parseListEntry(key, entry);
      receiver ??= videoParser.parseListEntry(key, entry);

      if (null != receiver) receivers.add(receiver);
    });

    return receivers;
  }
}
