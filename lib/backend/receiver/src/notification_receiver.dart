import 'receiver.dart';

class NotificationReceiver extends Receiver {
  NotificationReceiver({
    this.title = "",
    this.message = "",
    this.notificationId = 1,
    this.channel = NotificationChannel.defaultChannel,
  });

  String title;
  String message;
  int notificationId;
  NotificationChannel channel;

  @override
  Map<String, Object> get args => {
        getKey("_title"): title,
        getKey("_message"): message,
        getKey("_notificationId"): notificationId,
        getKey("_channelId"): channel.id,
      };

  /// All android permissions this Receiver needs in order to
  /// work properly.
  static Set<String> get permissions => {};

  @override
  String toString() =>
      "{$simpleClassName: [title: $title, message: $message, notificationId : $notificationId, channel: $channel]}";

  @override
  NotificationReceiver? parseListEntry(String key, dynamic entry) {
    if (simpleClassName != key) return null;
    return NotificationReceiver();
  }
}

class NotificationChannel {
  static const NotificationChannel defaultChannel = NotificationChannel(
      id: "test_alarm_manager", name: "test_alarm_manager channel");

  const NotificationChannel({
    required this.id,
    required this.name,
    this.importance = ChannelImportance.normal,
  });

  final String id;
  final String name;
  final ChannelImportance importance;
}

enum ChannelImportance { high, low, max, min, normal, none, unspecified }

extension _ChannelImportanceNativeValue on ChannelImportance {
  int get nativeValue {
    final int? importance = {
      ChannelImportance.max: 5,
      ChannelImportance.high: 4,
      ChannelImportance.normal: 3,
      ChannelImportance.low: 2,
      ChannelImportance.min: 1,
      ChannelImportance.none: 0,
      ChannelImportance.unspecified: -1000,
    }[this];

    return importance!;
  }
}
