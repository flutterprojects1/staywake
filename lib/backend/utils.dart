import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';

import 'assets.dart';

const String rootPackage = "com.u_12ampere.staywake";

Assets get soundAssets => _soundAssets;
Assets _soundAssets = Assets(prefix: "sounds");

Assets get videoAssets => _videoAssets;
Assets _videoAssets = Assets(prefix: "videos");

/// ## Description
///
/// *  Get a millisecond value that is an offset from the
///    `Epoch, January 1, 1970 00:00:00.000 GMT (Gregorian).`
///
///
/// ## Parameters
///
/// * `hourOfDay`   - The hour of day, has to be within [0;23].
///
///
/// * `minute`      - Has to be within [0;59].
///
///
/// * `autoCorrect` - If you call `getTimeInMillis(hourOfDay: 15, minute: 22)`,
///   but the current time is `Sat Aug 22 19:33:41 GMT 2020`, then `autoCorrect`
///   returns the timestamp of `Sun Aug 23 15:22:00 GMT 2020`, instead of
///   `Sat Aug 22 15:22:00 GMT 2020` in milliseconds.
///
///
/// ## References
///
/// * [DateTime]
///
/// * [TimeOfDay]
int getTimeInMillis({
  int hourOfDay = 0,
  int minute = 0,
  bool autoCorrect = true,
}) {
  assert(hourOfDay >= 0 && hourOfDay <= 23);
  assert(minute >= 0 && minute <= 59);

  if (autoCorrect) {
    final now = TimeOfDay.now();
    if ((hourOfDay * 60 + minute) <= (now.hour * 60 + now.minute))
      // ignore: parameter_assignments
      hourOfDay += 24;
  }

  final now = DateTime.now();
  return DateTime(
    now.year,
    now.month,
    now.day,
    hourOfDay,
    minute,
  ).millisecondsSinceEpoch;
}

/// ## Description
///
/// * This function randomly generates a [TimeOfDay], with its starting point
///   given by the parameters of [start] and a minimum difference in time of
///   [min] and a maximal difference in time of [max].
///
///
/// ## Parameters
///
/// * `start` - The point in time to which you want to add a random duration
///             between [min] and [max]. [start] defaults to TimeOfDay.now().
///
///
/// * `min`   - The minimum duration that has to be added to [start].
///
///
/// * `max`   - The maximal duration that can be added to [start].
///
///
/// * `seed`  - The seed used for the [Random] number generator.
///
///
/// ## Return
///
/// * A [TimeOfDay] object with its hour as `start.hour + randomHour` and
///   its minute as  `start.minute + randomMinute`.
TimeOfDay getRandomTimeOfDay({
  required Duration min,
  required Duration max,
  TimeOfDay? start,
  int? seed,
}) {
  start ??= TimeOfDay.now();
  final Random rand = (null == seed) ? Random() : Random(seed);

  final delta = max.inMilliseconds - min.inMilliseconds;

  int randomHourInMillis = 0;
  if (Duration.millisecondsPerHour <= delta) {
    randomHourInMillis =
        min.inMilliseconds + rand.nextInt(delta + Duration.millisecondsPerHour);
  }

  int randomMinuteInMillis = 0;
  if (Duration.millisecondsPerMinute <= delta) {
    randomMinuteInMillis = min.inMilliseconds +
        rand.nextInt(delta + Duration.millisecondsPerMinute);
  }

  int hour = start.hour + (((randomHourInMillis.toDouble() / 1000) / 60) ~/ 60);
  int minute = start.minute + ((randomMinuteInMillis.toDouble() / 1000) ~/ 60);

  if (60 <= minute) {
    hour++;
    minute -= 60;
  }

  if (23 <= hour) hour -= 24;

  return TimeOfDay(
    hour: hour,
    minute: minute,
  );
}

extension RandomElement<T> on List<T> {
  T getRandom([Random? random]) {
    random ??= Random();
    return this[random.nextInt(length)];
  }
}

extension WeekdayString on DateTime {
  String get weekdayAsString {
    late String day;

    switch (weekday) {
      case DateTime.monday:
        day = "Montag";
        break;

      case DateTime.tuesday:
        day = "Dienstag";
        break;

      case DateTime.wednesday:
        day = "Mittwoch";
        break;

      case DateTime.thursday:
        day = "Donnerstag";
        break;

      case DateTime.friday:
        day = "Freitag";
        break;

      case DateTime.saturday:
        day = "Samstag";
        break;

      case DateTime.sunday:
        day = "Sonntag";
        break;
    }

    return day;
  }

  /// Get the time in the format `hh:mm`.
  /// The maximum is `23:59` whereas the lowest is `00:00`.
  String get hourAndMinute {
    String time = "${hour < 10 ? "0" : ""}$hour";
    time += ":";
    time += "${minute < 10 ? "0" : ""}$minute";
    return time;
  }
}

/*-------------------------------------------------------------------------------------------*/
/*                                 Some pre-defined methods                                  */
/*-------------------------------------------------------------------------------------------*/

/// Loads all assets from the `assets/` directory.
Future<void> loadAllAssets([bool showLog = false]) async {
  await _soundAssets.loadAll([
    "10k.mp3",
    "11k.mp3",
    "12k.mp3",
    "13k.mp3",
    "14k.mp3",
    "15k.mp3",
    "16k.mp3",
    "17k.mp3",
    "18k.mp3",
    "test.mp3"
  ]);

  await _videoAssets.loadAll([
    "1.mp4",
    "2.mp4",
    "3.mp4",
    "4.mp4",
    "5.mp4",
    "6.mp4",
    "7.mp4",
    "8.mp4",
  ]);

  if (showLog) {
    soundAssets.loadedFiles.forEach((name, file) {
      print(
          '${'\nname-noprefix: ${soundAssets.getFileNameWithOutPrefix(name)}'}${'\nname: $name'}${'\nfile path: ${file.path}\n'}');
    });

    videoAssets.loadedFiles.forEach((name, file) {
      print(
          '${'\nname-noprefix: ${videoAssets.getFileNameWithOutPrefix(name)}'}${'\nname: $name'}${'\nfile path: ${file.path}\n'}');
    });
  }
}

extension SimpleClassName on Object {
  String get simpleClassName => runtimeType.toString();
}

/// percent has to be within [0;1]
double getWindowWidth({
  required BuildContext context,
  required double percent,
}) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.width * percent;
}

/// percent has to be within [0;1]
double getWindowHeight({
  required BuildContext context,
  required double percent,
}) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.height * percent;
}

Future<Uint8List> loadImage(String url) {
  late ImageStreamListener listener;

  final Completer<Uint8List> completer = Completer<Uint8List>();
  final ImageStream imageStream =
      AssetImage(url).resolve(ImageConfiguration.empty);

  listener = ImageStreamListener(
    (imageInfo, synchronousCall) {
      imageInfo.image.toByteData(format: ImageByteFormat.png).then((byteData) {
        imageStream.removeListener(listener);
        completer.complete(byteData?.buffer.asUint8List());
      });
    },
    onError: (exception, stackTrace) {
      imageStream.removeListener(listener);
      completer.completeError(exception);
    },
  );

  imageStream.addListener(listener);

  return completer.future;
}
